<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Area extends CI_Controller 
{
	public function __construct(){
		parent::__construct();
		$this->load->model('Areas_model');
	}

	public function index()
	{

		$this->validAccess();
		$data['session'] = $this->session->userdata('up_session');
		$data['areas'] = $this->Areas_model->loadAreas();
		$this->load->view('areas',$data);
	}

	public function saveAreas()
	{
		$this->form_validation->set_rules('nombreArea','Nombre','required|max_length[40]');		

		if($this->form_validation->run()==FALSE)
		{
			$data['areas'] = $this->Areas_model->loadAreas();
			$this->session->set_flashdata('areaSaveError','Error saving information, valid fields');

			$data['areas'] = $this->Areas_model->loadAreas();
			$this->load->view('areas',$data);

		}
		else{
			$data = array(
			"nombreArea"=>$this->input->post('nombreArea'),
			"statusArea"=>"Activa",
			);

			if($this->input->post('idArea'))
			{
				
				$idArea = $this->input->post('idArea');

				$this->Areas_model->editAreas($data,$idArea);
				$this->session->set_flashdata('areaSaveSuccess','Information edited successfully');
				redirect('Area');
			}
			else
			{
				$this->Areas_model->saveAreas($data);
				$this->session->set_flashdata('areaSaveSuccess','Information saved successfully');
				redirect('Area');
			}
		}
	}

	public function editAreas($idArea=NULL){
		if($idArea!=NULL){
			$areaToEdit = $this->Areas_model->getAreasById($idArea);
			if($areaToEdit==NULL){
				$this->session->set_flashdata('areaSaveError','Event not found');
				redirect('Area');
			}else{
				$data['areas'] = $this->Areas_model->loadAreas();
				$data['areaToEdit'] = $areaToEdit;
				$this->load->view('areas',$data);
			}
			
		}else{
			$this->session->set_flashdata('areaSaveError','Event not found');
			redirect('Area');
		}
	}

	function editStatusArea($idArea=NULL){
		if ($idArea!=null){
			$areaToEdit = $this->Areas_model->loadAreasById($idArea);
			if ($areaToEdit==NULL){
				$this->session->set_flashdata('areaSaveError','Event not found');
				redirect('Area');
			} else{
				if($areaToEdit->statusArea=="Activa"){
					$data = array(
						"statusArea"  =>"Inactiva"
					);
				} else if($areaToEdit->statusArea=="Inactiva"){
					$data = array(
						"statusArea" =>"Activa"
					);
				}

				$this->Areas_model->editEstatus($data,$idArea);
				$this->session->set_flashdata('areaSaveSuccess','Information saved successfully');
				redirect('Area');
			}
		} else{
			$this->session->set_flashdata('areaSaveError','Event not found');
			redirect('Area');
		}
	}

	function validAccess() {
		$s =$this->session->userdata('up_session');
		if (!@$s['email'] || !@$s['isLogged']) {
			redirect('Login');
		}
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tarjeton extends CI_Controller 
{
	public function __construct(){
		parent::__construct();
		$this->load->model('tarjeton_model');
	}

	public function index()
	{
		$this->validAccess();
		$data['tarjetones'] = $this->tarjeton_model->loadTarjeton();
		$this->load->view('tarjeton_view',$data);
	}

	public function saveTarjeton()
	{
		$this->load->library('form_validation');
		$data['session'] = $this->session->userdata('up_session');
		$this->form_validation->set_rules('vigenciaTarjeton','Vigencia','required');
		$this->form_validation->set_rules('noTarjeton','Numero','required|max_length[15]|min_length[4]');
		$this->form_validation->set_rules('tipoTarjeton','Tipo','required');
		$this->form_validation->set_rules('colorTarjeton','Color','required');
		if($this->form_validation->run()==FALSE)
		{
			$data['tarjetones'] = $this->tarjeton_model->loadTarjeton();
			$this->session->set_flashdata('tarjetonSaveError','Error saving information, valid fields');

			$data['tarjetones'] = $this->tarjeton_model->loadTarjeton();
			$this->load->view('tarjeton_view',$data);

		}
		else{
			$data = array(
			"vigenciaTarjeton"=>$this->input->post('vigenciaTarjeton'),
			"statusTarjeton"=>"Activo",
			"noTarjeton"=>$this->input->post('noTarjeton'),
			"tipoTarjeton"=>$this->input->post('tipoTarjeton'),
			"colorTarjeton"=>$this->input->post('colorTarjeton'),
			);

			if($this->input->post('idTarjeton'))
			{
				
				$idTarjeton = $this->input->post('idTarjeton');

				$this->tarjeton_model->editTarjeton($data,$idTarjeton);
				$this->session->set_flashdata('tarjetonSaveSuccess','Information edited successfully');
				redirect('Tarjeton');
			}
			else
			{
				$this->tarjeton_model->saveTarjeton($data);
				$this->session->set_flashdata('tarjetonSaveSuccess','Information saved successfully');
				redirect('Tarjeton');
			}
		}
	}

	public function editTarjetones($idTarjeton=NULL){
		if($idTarjeton!=NULL){
			$tarjetonToEdit = $this->tarjeton_model->getTarjetonById($idTarjeton);
			if($tarjetonToEdit==NULL){
				$this->session->set_flashdata('tarjetonSaveError','Event not found');
				redirect('tarjeton_view');
			}else{
				$data['tarjetones'] = $this->tarjeton_model->loadTarjeton();
				$data['tarjetonToEdit'] = $tarjetonToEdit;
				$this->load->view('tarjeton_view',$data);
			}
			
		}else{
			$this->session->set_flashdata('tarjetonSaveError','Event not found');
			redirect('tarjeton_view');
		}
	}

	function editStatusTarjetones($idTarjeton=NULL){
		if ($idTarjeton!=null){
			$tarjetonToEdit = $this->tarjeton_model->loadTarjeById($idTarjeton);
			if ($tarjetonToEdit==NULL){
				$this->session->set_flashdata('tarjetonSaveError','Event not found');
				redirect('Tarjeton'); //Si no cambiar al Tarjeton
			} else{
				if($tarjetonToEdit->statusTarjeton=="Activo"){
					$data = array(
						"statusTarjeton"  =>"Inactivo"
					);
				} else if($tarjetonToEdit->statusTarjeton=="Inactivo"){
					$data = array(
						"statusTarjeton" =>"Activo"
					);
				}

				$this->tarjeton_model->editEstatus($data,$idTarjeton);
				$this->session->set_flashdata('tarjetonSaveSuccess','Information saved successfully');
				redirect('Tarjeton');
			}
		} else{
			$this->session->set_flashdata('tarjetonSaveError','Event not found');
			redirect('Tarjeton');
		}
	}

	function validAccess() {
		$s =$this->session->userdata('up_session');
		if (!@$s['email'] || !@$s['isLogged']) {
			redirect('Login');
		}
	}
}
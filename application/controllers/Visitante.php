<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Visitante extends CI_Controller 
{
	public function __construct(){
		parent::__construct();
		$this->load->model('visitante_model');
	}
	
	public function addInfoNew()
	{
		$data['session'] = $this->session->userdata('up_session');
		$data['visitantes'] = $this->visitante_model->loadVisitante();
		$data['areas'] = $this->visitante_model->loadAreas();
		$data['tipoId'] = $this->visitante_model->loadIdentificaciones();
		$data['persona'] = $this->visitante_model->loadLastPersona();

		$this->load->view('visitante_view',$data);
	}
	public function addInfo($idPersona=NULL)
	{
		$data['session'] = $this->session->userdata('up_session');
		$data['persona'] = $this->visitante_model->getPersonaById($idPersona);
		$data['visitantes'] = $this->visitante_model->loadVisitante();
		$data['areas'] = $this->visitante_model->loadAreas();
		$data['tipoId'] = $this->visitante_model->loadIdentificaciones();
		$this->load->view('visitante_view',$data);

	}
	public function index()
	{
		$data['session'] = $this->session->userdata('up_session');
		$this->validAccess();
		$data['visitantes'] = $this->visitante_model->loadVisitante();
		$data['areas'] = $this->visitante_model->loadAreas();
		$data['tipoId'] = $this->visitante_model->loadIdentificaciones();

		$this->load->view('visitante_view',$data);
	}

	public function saveVisitante()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nombreVisitante','Nombre','required|max_length[50]|min_length[4]');
		$this->form_validation->set_rules('apellidosVisitante','Apellido','required|max_length[50]|min_length[4]');
		$this->form_validation->set_rules('fechaNacVisitante','Fecha de nacimiento','required');
		$this->form_validation->set_rules('generoVisitante','Genero','required');
		$this->form_validation->set_rules('areaVisitante','Area','required');
		$this->form_validation->set_rules('tipoIdVisitante','Tipo de identificacion','required');
		$this->form_validation->set_rules('codigoIdentificacion','Codigo','required|max_length[50]|min_length[4]');
		$this->form_validation->set_rules('personaVisitante','Persona','required');
		if($this->form_validation->run()==FALSE)
		{
			$data['visitantes'] = $this->visitante_model->loadVisitante();
			$this->session->set_flashdata('visitanteSaveError','Error saving information, valid fields');

			$data['visitantes'] = $this->visitante_model->loadVisitante();
			$this->load->view('visitante_view',$data);

		}
		else{
			$data = array(
			"nombreVisitante"=>$this->input->post('nombreVisitante'),
			"apellidosVisitante"=>$this->input->post('apellidosVisitante'),
			"fechaNacVisitante"=>$this->input->post('fechaNacVisitante'),
			"generoVisitante"=>$this->input->post('generoVisitante'),
			"fktipoIdVisitante"=>$this->input->post('tipoIdVisitante'),
			"codigoIdVisitante"=>$this->input->post('codigoIdentificacion'),
			"fkareaVisitante"=>$this->input->post('areaVisitante'),
			"fkPersonaVisitante"=>$this->input->post('personaVisitante'),
			);

			if($this->input->post('idVisitante'))
			{
				
				$idVisitante = $this->input->post('idVisitante');

				$this->visitante_model->editVisitante($data,$idVisitante);
				$this->session->set_flashdata('visitanteSaveSuccess','Information edited successfully');
				redirect('Visitante');
			}
			else
			{
				$this->visitante_model->saveVisitante($data);
				$this->session->set_flashdata('visitanteSaveSuccess','Information saved successfully');
				redirect('Visitante');
			}
		}
	}

	public function editVisitantes($idVisitante=NULL){
		if($idVisitante!=NULL){
			$visitanteToEdit = $this->visitante_model->getVisitanteById($idVisitante);
			if($visitanteToEdit==NULL){
				$this->session->set_flashdata('visitanteSaveError','Event not found');
				redirect('visitante_view');
			}else{
				$data['visitantes'] = $this->visitante_model->loadVisitante();
				$data['carreras'] = $this->visitante_model->loadCarreras();
				$data['grupos'] = $this->visitante_model->loadGrupos();
				$data['tipoId'] = $this->visitante_model->loadIdentificaciones();
				$data['visitanteToEdit'] = $visitanteToEdit;
				$this->load->view('visitante_view',$data);
			}
			
		}else{
			$this->session->set_flashdata('visitanteSaveError','Event not found');
			redirect('visitante_view');
		}
	}

	function validAccess() {
		$s =$this->session->userdata('up_session');
		if (!@$s['email'] || !@$s['isLogged']) {
			redirect('Login');
		}
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mensaje extends CI_Controller 
{
	public function __construct(){
		parent::__construct();
		$this->load->model('Mensajes_model');
	}

	public function index()
	{
		$this->validAccess();
		$data['session'] = $this->session->userdata('up_session');
		$data['session'] = $this->session->userdata('up_session');
		$data['mensajes'] = $this->Mensajes_model->loadMensajes();
		$data['personas_control'] = $this->Mensajes_model->loadPersonas();
		$this->load->view('mensajes',$data);
	}

	public function saveMensajes()
	{
		$this->form_validation->set_rules('fkPersonaMensaje','Persona','required');

		$this->form_validation->set_rules('tipoMensaje','Tipo','required');

		$this->form_validation->set_rules('tituloMensaje','Titulo','required|min_length[8]|max_length[50]');

		$this->form_validation->set_rules('motivoMensaje','Motivo','required|min_length[15]|max_length[300]');			

		if($this->form_validation->run()==FALSE)
		{
			$data['mensajes'] = $this->Mensajes_model->loadMensajes();
			$this->session->set_flashdata('mensajeSaveError','Error saving information, valid fields');

			$data['mensajes'] = $this->Mensajes_model->loadMensajes();
			$this->load->view('mensajes',$data);

		}
		else{
			$data = array(
			"fkPersonaMensaje"=>$this->input->post('fkPersonaMensaje'),
			"tipoMensaje"=>$this->input->post('tipoMensaje'),
			"tituloMensaje"=>$this->input->post('tituloMensaje'),
			"motivoMensaje"=>$this->input->post('motivoMensaje'),

			//"statusArea"=>"Activa",
			);

			if($this->input->post('idMensaje'))
			{
				
				$idMensaje = $this->input->post('idMensaje');

				$this->Mensajes_model->editMensajes($data,$idMensaje);
				$this->session->set_flashdata('mensajeSaveSuccess','Information edited successfully');
				redirect('Mensaje');
			}
			else
			{
				$this->Mensajes_model->saveMensajes($data);
				$this->session->set_flashdata('mensajeSaveSuccess','Information saved successfully');
				redirect('Mensaje');
			}
		}
	}

	public function editMensajes($idMensaje=NULL){
		if($idMensaje!=NULL){
			$mensajeToEdit = $this->Mensajes_model->getMensajesById($idMensaje);
			if($mensajeToEdit==NULL){
				$this->session->set_flashdata('mensajeSaveError','Event not found');
				redirect('Mensaje');
			}else{
				$data['mensajes'] = $this->Mensajes_model->loadMensajes();
				$data['personas_control'] = $this->Mensajes_model->loadPersonas();
				$data['mensajeToEdit'] = $mensajeToEdit;
				$this->load->view('mensajes',$data);
			}
			
		}else{
			$this->session->set_flashdata('mensajeSaveError','Event not found');
			redirect('Mensaje');
		}
	}

	function editStatusMensaje($idMensaje=NULL){
		if ($idMensaje!=null){
			$mensajeToEdit = $this->Mensajes_model->loadMensajesById($idMensaje);
			if ($mensajeToEdit==NULL){
				$this->session->set_flashdata('mensajeSaveError','Event not found');
				redirect('Mensaje');
			} else{
				if($mensajeToEdit->fueEntregadoMensaje=="Si"){
					$data = array(
						"fueEntregadoMensaje"  =>"No"
					);
					$this->session->set_flashdata('mensajeSaveSuccess','Information saved successfully');
				redirect('Mensaje');
				} else if($mensajeToEdit->fueEntregadoMensaje=="No"){
					$data = array(
						"fueEntregadoMensaje" =>"Si",
					);
					$this->Mensajes_model->editEstatus($data,$idMensaje);
					$this->session->set_flashdata('mensajeSaveSuccess','Information saved successfully');
				redirect('Mensaje');
				}

				//$this->Mensajes_model->editEstatus($data,$idMensaje);
				
			}
		} else{
			$this->session->set_flashdata('mensajeSaveError','Event not found');
			redirect('Mensaje');
		}
	}

	function validAccess() {
		$s =$this->session->userdata('up_session');
		if (!@$s['email'] || !@$s['isLogged']) {
			redirect('Login');
		}
	}
}
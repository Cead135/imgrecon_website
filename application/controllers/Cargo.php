<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cargo extends CI_Controller 
{
	public function __construct(){
		parent::__construct();
		$this->load->model('Cargos_model');
	}

	public function index()
	{
		$this->validAccess();
		$data['session'] = $this->session->userdata('up_session');
		$data['cargos'] = $this->Cargos_model->loadCargos();
		$this->load->view('cargos',$data);
	}

	public function saveCargos()
	{
		$this->form_validation->set_rules('NombreCargo','Nombre','required|max_length[60]');

		if($this->form_validation->run()==FALSE)
		{
			$data['cargos'] = $this->Cargos_model->loadCargos();
			$this->session->set_flashdata('cargoSaveError','Error saving information, valid fields');

			$data['cargos'] = $this->Cargos_model->loadCargos();
			$this->load->view('cargos',$data);

		}
		else{
			$data = array(
			"NombreCargo"=>$this->input->post('NombreCargo'),
			"EstatusCargo"=>"Activo",
			);

			if($this->input->post('idCargoAdministrativo'))
			{
				
				$idCargoAdministrativo = $this->input->post('idCargoAdministrativo');

				$this->Cargos_model->editCargos($data,$idCargoAdministrativo);
				$this->session->set_flashdata('cargoSaveSuccess','Information edited successfully');
				redirect('Cargo');
			}
			else
			{
				$this->Cargos_model->saveCargos($data);
				$this->session->set_flashdata('cargoSaveSuccess','Information saved successfully');
				redirect('Cargo');
			}
		}
	}

	public function editCargos($idCargoAdministrativo=NULL){
		if($idCargoAdministrativo!=NULL){
			$cargoToEdit = $this->Cargos_model->getCargosById($idCargoAdministrativo);
			if($cargoToEdit==NULL){
				$this->session->set_flashdata('cargoSaveError','Event not found');
				redirect('Cargo');
			}else{
				$data['cargos'] = $this->Cargos_model->loadCargos();
				$data['cargoToEdit'] = $cargoToEdit;
				$this->load->view('cargos',$data);
			}
			
		}else{
			$this->session->set_flashdata('cargoSaveError','Event not found');
			redirect('Cargo');
		}
	}

	function editStatusCargo($idCargoAdministrativo=NULL){
		if ($idCargoAdministrativo!=null){
			$cargoToEdit = $this->Cargos_model->loadCargosById($idCargoAdministrativo);
			if ($cargoToEdit==NULL){
				$this->session->set_flashdata('cargoSaveError','Event not found');
				redirect('Cargo');
			} else{
				if($cargoToEdit->EstatusCargo=="Activo"){
					$data = array(
						"EstatusCargo"  =>"Inactivo"
					);
				} else if($cargoToEdit->EstatusCargo=="Inactivo"){
					$data = array(
						"EstatusCargo" =>"Activo"
					);
				}

				$this->Cargos_model->editEstatus($data,$idCargoAdministrativo);
				$this->session->set_flashdata('cargoSaveSuccess','Information saved successfully');
				redirect('Cargo');
			}
		} else{
			$this->session->set_flashdata('cargoSaveError','Event not found');
			redirect('Cargo');
		}
	}

	function validAccess() {
		$s =$this->session->userdata('up_session');
		if (!@$s['email'] || !@$s['isLogged']) {
			redirect('Login');
		}
	}
}
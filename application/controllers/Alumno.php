<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alumno extends CI_Controller 
{
	public function __construct(){
		parent::__construct();
		$this->load->model('alumno_model');
		$this->load->model('Carreras_model');
		$this->load->model('Grupos_model');
		$this->load->model('Tipoiden_model');
		$this->load->model('domicilio_model');
		$this->load->model('personas_model');
	}
	public function addInfoNew()
	{
		$data['session'] = $this->session->userdata('up_session');
		$data['alumnos'] = $this->alumno_model->loadAlumno();
		$data['carreras'] = $this->alumno_model->loadCarreras();
		$data['grupos'] = $this->alumno_model->loadGrupos();
		$data['tipoId'] = $this->alumno_model->loadIdentificaciones();
		$data['domicilio'] = $this->alumno_model->loadDomicilio();
		$data['persona'] = $this->alumno_model->loadLastPersona();

		$this->load->view('alumno_view',$data);
	}
	public function addInfo($idPersona=NULL)
	{
		$data['session'] = $this->session->userdata('up_session');
		$data['persona'] = $this->alumno_model->getPersonaById($idPersona);
		$data['alumnos'] = $this->alumno_model->loadAlumno();
		$data['carreras'] = $this->alumno_model->loadCarreras();
		$data['grupos'] = $this->alumno_model->loadGrupos();
		$data['tipoId'] = $this->alumno_model->loadIdentificaciones();
		$data['domicilio'] = $this->alumno_model->loadDomicilio();
		$this->load->view('alumno_view',$data);

	}
	public function index()
	{
		$data['session'] = $this->session->userdata('up_session');
		$this->validAccess();
		$data['alumnos'] = $this->alumno_model->loadAlumno();
		$data['carreras'] = $this->alumno_model->loadCarreras();
		$data['grupos'] = $this->alumno_model->loadGrupos();
		$data['tipoId'] = $this->alumno_model->loadIdentificaciones();
		$data['domicilio'] = $this->alumno_model->loadDomicilio();

		$this->load->view('alumno_view',$data);
	}

	public function saveAlumno()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nombreAlumno','Nombre','required|max_length[50]|min_length[4]');
		$this->form_validation->set_rules('apellidoAlumno','Apellido','required|max_length[50]|min_length[4]');
		$this->form_validation->set_rules('fechaNacAlumno','Fecha de nacimiento','required');
		$this->form_validation->set_rules('generoAlumno','Genero','required');
		$this->form_validation->set_rules('curpAlumno','CURP','required|max_length[20]|min_length[4]');
		$this->form_validation->set_rules('carreraAlumno','Carrera','required');
		$this->form_validation->set_rules('grupoAlumno','Grupo','required');
		$this->form_validation->set_rules('matriculaAlumno','Matricula','required|max_length[20]|min_length[4]');
		$this->form_validation->set_rules('tipoIdAlumno','Tipo de identificacion','required');
		$this->form_validation->set_rules('noIdentificacionAumno','Codigo','required|max_length[50]|min_length[4]');
		//$this->form_validation->set_rules('domicilioAlumno','Domicilio','required');
		$this->form_validation->set_rules('personaAlumno','Persona','required');
		if($this->form_validation->run()==FALSE)
		{
			$data['alumnos'] = $this->alumno_model->loadAlumno();
			$this->session->set_flashdata('alumnoSaveError','Error saving information, valid fields');

			$data['alumnos'] = $this->alumno_model->loadAlumno();
			$this->load->view('alumno_view',$data);

		}
		else{
			$data = array(
			"nombreAlumno"=>$this->input->post('nombreAlumno'),
			"apellidoAlumno"=>$this->input->post('apellidoAlumno'),
			"fechaNacAlumno"=>$this->input->post('fechaNacAlumno'),
			"generoAlumno"=>$this->input->post('generoAlumno'),
			"curpAlumno"=>$this->input->post('curpAlumno'),
			"fkCarreraAlumno"=>$this->input->post('carreraAlumno'),
			"fkGrupoAlumno"=>$this->input->post('grupoAlumno'),
			"matriculaAlumno"=>$this->input->post('matriculaAlumno'),
			"fkTipoIdentificacionAlumno"=>$this->input->post('tipoIdAlumno'),
			"noIdentificacionAumno"=>$this->input->post('noIdentificacionAumno'),
			"fkPersonaAlumno"=>$this->input->post('personaAlumno'),
			);

			if($this->input->post('idAlumno'))
			{
				
				$idAlumno = $this->input->post('idAlumno');

				$this->alumno_model->editAlumno($data,$idAlumno);
				$this->session->set_flashdata('alumnoSaveSuccess','Information edited successfully');
				redirect('Alumno');
			}
			else
			{
				$this->alumno_model->saveAlumno($data);
				$this->session->set_flashdata('alumnoSaveSuccess','Information saved successfully');
				redirect('Alumno');
			}
		}
	}

	public function editAlumnos($idAlumno=NULL){
		if($idAlumno!=NULL){
			$alumnoToEdit = $this->alumno_model->getAlumnoById($idAlumno);
			if($alumnoToEdit==NULL){
				$this->session->set_flashdata('alumnoSaveError','Event not found');
				redirect('alumno_view');
			}else{
				$data['alumnos'] = $this->alumno_model->loadAlumno();
				$data['carreras'] = $this->alumno_model->loadCarreras();
				$data['grupos'] = $this->alumno_model->loadGrupos();
				$data['tipoId'] = $this->alumno_model->loadIdentificaciones();
				$data['alumnoToEdit'] = $alumnoToEdit;
				$this->load->view('alumno_view',$data);
			}
			
		}else{
			$this->session->set_flashdata('alumnoSaveError','Event not found');
			redirect('alumno_view');
		}
	}

	function editStatusAlumnos($idAlumno=NULL){
		if ($idAlumno!=null){
			$alumnoToEdit = $this->alumno_model->loadTarjeById($idAlumno);
			if ($alumnoToEdit==NULL){
				$this->session->set_flashdata('alumnoSaveError','Event not found');
				redirect('Alumno');
			} else{
				if($alumnoToEdit->statusAlumno=="Activo"){
					$data = array(
						"statusAlumno"  =>"Inactivo"
					);
				} else if($alumnoToEdit->statusAlumno=="Inactivo"){
					$data = array(
						"statusAlumno" =>"Activo"
					);
				}

				$this->alumno_model->editEstatus($data,$idAlumno);
				$this->session->set_flashdata('alumnoSaveSuccess','Information saved successfully');
				redirect('Alumno');
			}
		} else{
			$this->session->set_flashdata('alumnoSaveError','Event not found');
			redirect('Alumno');
		}
	}
	function validAccess() {
		$s =$this->session->userdata('up_session');
		if (!@$s['email'] || !@$s['isLogged']) {
			redirect('Login');
		}
	}
}
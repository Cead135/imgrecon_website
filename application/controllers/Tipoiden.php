<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipoiden extends CI_Controller 
{
	public function __construct(){
		parent::__construct();
		$this->load->model('Tipoiden_model');
	}

	public function index()
	{
		$this->validAccess();
		$data['session'] = $this->session->userdata('up_session');
		$data['tipoiden'] = $this->Tipoiden_model->loadIdentificaciones();
		$this->load->view('tipoiden',$data);
	}

	public function saveIdentificaciones()
	{
		$this->form_validation->set_rules('nombreIdentificacion','Nombre','required|max_length[30]');

		if($this->form_validation->run()==FALSE)
		{
			$data['tipoiden'] = $this->Tipoiden_model->loadIdentificaciones();
			$this->session->set_flashdata('identificacionSaveError','Error saving information, valid fields');

			$data['tipoiden'] = $this->Tipoiden_model->loadIdentificaciones();
			$this->load->view('tipoiden',$data);

		}
		else{
			$data = array(
			"nombreIdentificacion"=>$this->input->post('nombreIdentificacion'),
			"statusIdentificacion"=>"Activo",
			);

			if($this->input->post('idIdentificacion'))
			{
				
				$idIdentificacion = $this->input->post('idIdentificacion');

				$this->Tipoiden_model->editIdentificaciones($data,$idIdentificacion);
				$this->session->set_flashdata('identificacionSaveSuccess','Information edited successfully');
				redirect('Tipoiden');
			}
			else
			{
				$this->Tipoiden_model->saveIdentificaciones($data);
				$this->session->set_flashdata('identificacionSaveSuccess','Information saved successfully');
				redirect('Tipoiden');
			}
		}
	}

	public function editIdentificaciones($idIdentificacion=NULL){
		if($idIdentificacion!=NULL){
			$identificacionToEdit = $this->Tipoiden_model->getIdentificacionesById($idIdentificacion);
			if($identificacionToEdit==NULL){
				$this->session->set_flashdata('identificacionSaveError','Event not found');
				redirect('Tipoiden');
			}else{
				$data['tipoiden'] = $this->Tipoiden_model->loadIdentificaciones();
				$data['identificacionToEdit'] = $identificacionToEdit;
				$this->load->view('tipoiden',$data);
			}
			
		}else{
			$this->session->set_flashdata('identificacionSaveError','Event not found');
			redirect('Tipoiden');
		}
	}

	function editStatusIdentificaciones($idIdentificacion=NULL){
		if ($idIdentificacion!=null){
			$identificacionToEdit = $this->Tipoiden_model->loadIdenById($idIdentificacion);
			if ($identificacionToEdit==NULL){
				$this->session->set_flashdata('identificacionSaveError','Event not found');
				redirect('Tipoiden');
			} else{
				if($identificacionToEdit->statusIdentificacion=="Activo"){
					$data = array(
						"statusIdentificacion"  =>"Inactivo"
					);
				} else if($identificacionToEdit->statusIdentificacion=="Inactivo"){
					$data = array(
						"statusIdentificacion" =>"Activo"
					);
				}

				$this->Tipoiden_model->editEstatus($data,$idIdentificacion);
				$this->session->set_flashdata('identificacionSaveSuccess','Information saved successfully');
				redirect('Tipoiden');
			}
		} else{
			$this->session->set_flashdata('identificacionSaveError','Event not found');
			redirect('Tipoiden');
		}
	}

	function validAccess() {
		$s =$this->session->userdata('up_session');
		if (!@$s['email'] || !@$s['isLogged']) {
			redirect('Login');
		}
	}
}
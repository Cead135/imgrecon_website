<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class personas_control extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('personas_model');
	}
	public function addAddress($idPersona=NULL)
	{
		if($idPersona!=NULL)
		{
			redirect('Domicilio/newDomicilio/'.$idPersona);
		}
	}

	public function index(){
		$this->validAccess();
		$data['session'] = $this->session->userdata('up_session');
		$data['persona'] = $this->personas_model->loadPersonas();
		$this->load->view('personas_view',$data);
	}

	public function savePersonas(){

		$this->form_validation->set_rules('nombrePersona','Nombre','required|min_length[4]|max_length[20]');
		$this->form_validation->set_rules('apellidosPersona','Apellidos','required|min_length[4]|max_length[20]');
		$this->form_validation->set_rules('correoPersona','E-mail','required|min_length[4]|max_length[20]');
		$this->form_validation->set_rules('generoPersona','Gen�ro','required');
		$this->form_validation->set_rules('tipoPersona','Tipo','required');


		if($this->form_validation->run()==FALSE){
			$data['persona'] = $this->personas_model->loadPersonas();

			$this->session->set_flashdata('gameSaveError','Error saving information, valid fields');
			$this->load->view('personas_view',$data);

		}else{
			
			$data = array(
			"nombrePersona"=>$this->input->post('nombrePersona'),
			"apellidosPersona"=>$this->input->post('apellidosPersona'),
			"correoPersona"=>$this->input->post('correoPersona'),
			"generoPersona"=>$this->input->post('generoPersona'),			
			);

			if($this->input->post('idPersona')){
				
				$idPersona = $this->input->post('idPersona');

				$this->personas_model->editPersonas($data,$idPersona);
				$this->session->set_flashdata('gameSaveSuccess','Information edited successfully');
				redirect('personas_control');
			}else{

				$this->personas_model->savePersonas($data);
				//$data['lastPersona'] = $this->personas_model->loadLastPersona();
				switch ($this->input->post('tipoPersona')) {
					case 'Alumno':
					redirect('Alumno/addInfoNew',$data);
					break;
					case 'Empleado':
					redirect('Empleado/addInfoNew',$data);
					break;
					case 'Visitante':
					redirect('Visitante/addInfoNew',$data);
					break;
				}
				$this->session->set_flashdata('gameSaveSuccess','Information saved successfully');
			}
		}
		
	}
	public function addInfo($idPersona=NULL)
	{
		if($idPersona!=NULL){
			$toAdd = $this->personas_model->getPersonaById($idPersona);

			switch ($toAdd->tipoPersona) {
				case 'Alumno':
				redirect('Alumno/addInfo/'.$idPersona);
				break;
				case 'Empleado':
				redirect('Empleado/addInfo/'.$idPersona);
				break;
				case 'Visitante':
				redirect('Visitante/addInfo/'.$idPersona);
				break;
			}
		}
	}
	public function editPersonas($idPersona=NULL){
		if($idPersona!=NULL){
			$personaToEdit = $this->personas_model->getPersonaById($idPersona);
			if($personaToEdit==NULL){
				$this->session->set_flashdata('gameSaveError','Person not found');
				redirect('personas_control');
			}else{
				$data['persona'] = $this->personas_model->loadPersonas();
				$data['personaToEdit'] = $personaToEdit;
				$this->load->view('personas_view',$data);
			}
			
		}else{
			$this->session->set_flashdata('gameSaveError','Person not found');
			redirect('personas_control');
		}
	}

	function validAccess() {
		$s =$this->session->userdata('up_session');
		if (!@$s['email'] || !@$s['isLogged']) {
			redirect('Login');
		}
	}
}






<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupo extends CI_Controller 
{
	public function __construct(){
		parent::__construct();
		$this->load->model('Grupos_model');
	}

	public function index()
	{
		$this->validAccess();
		$data['session'] = $this->session->userdata('up_session');
		$data['grupos'] = $this->Grupos_model->loadGrupos();
		$data['carrera'] = $this->Grupos_model->loadCarreras();
		$this->load->view('grupos',$data);
	}

	public function saveGrupos()
	{
		$this->form_validation->set_rules('nombreGrupo','Nombre','required|max_length[20]');

		$this->form_validation->set_rules('fkcarrera','Carrera','required');		

		if($this->form_validation->run()==FALSE)
		{
			$data['grupos'] = $this->Grupos_model->loadGrupos();
			$this->session->set_flashdata('grupoSaveError','Error saving information, valid fields');

			$data['grupos'] = $this->Grupos_model->loadGrupos();
			$this->load->view('grupos',$data);

		}
		else{
			$data = array(
			"nombreGrupo"=>$this->input->post('nombreGrupo'),
			"fkcarrera"=>$this->input->post('fkcarrera'),
			"statusGrupo"=>"Activo",
			);

			if($this->input->post('idGrupo'))
			{
				
				$idGrupo = $this->input->post('idGrupo');

				$this->Grupos_model->editGrupos($data,$idGrupo);
				$this->session->set_flashdata('grupoSaveSuccess','Information edited successfully');
				redirect('Grupo');
			}
			else
			{
				$this->Grupos_model->saveGrupos($data);
				$this->session->set_flashdata('grupoSaveSuccess','Information saved successfully');
				redirect('Grupo');
			}
		}
	}

	public function editGrupos($idGrupo=NULL){
		if($idGrupo!=NULL){
			$grupoToEdit = $this->Grupos_model->getGruposById($idGrupo);
			if($grupoToEdit==NULL){
				$this->session->set_flashdata('grupoSaveError','Event not found');
				redirect('Grupo');
			}else{
				$data['grupos'] = $this->Grupos_model->loadGrupos();
				$data['carrera'] = $this->Grupos_model->loadCarreras();
				$data['grupoToEdit'] = $grupoToEdit;
				$this->load->view('grupos',$data);
			}
			
		}else{
			$this->session->set_flashdata('grupoSaveError','Event not found');
			redirect('Grupo');
		}
	}

	function editStatusGrupo($idGrupo=NULL){
		if ($idGrupo!=null){
			$grupoToEdit = $this->Grupos_model->loadGruposById($idGrupo);
			if ($grupoToEdit==NULL){
				$this->session->set_flashdata('grupoSaveError','Event not found');
				redirect('Grupo');
			} else{
				if($grupoToEdit->statusGrupo=="Activo"){
					$data = array(
						"statusGrupo"  =>"Inactivo"
					);
				} else if($grupoToEdit->statusGrupo=="Inactivo"){
					$data = array(
						"statusGrupo" =>"Activo"
					);
				}

				$this->Grupos_model->editEstatus($data,$idGrupo);
				$this->session->set_flashdata('grupoSaveSuccess','Information saved successfully');
				redirect('Grupo');
			}
		} else{
			$this->session->set_flashdata('grupoSaveError','Event not found');
			redirect('Grupo');
		}
	}

	function validAccess() {
		$s =$this->session->userdata('up_session');
		if (!@$s['email'] || !@$s['isLogged']) {
			redirect('Login');
		}
	}
}
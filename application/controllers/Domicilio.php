<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Domicilio extends CI_Controller 
{
	public function __construct(){
		parent::__construct();
		$this->load->model('domicilio_model');
	}
	public function newDomicilio($idPersona=NULL)
	{
		$data['session'] = $this->session->userdata('up_session');
		$data['persona'] = $this->domicilio_model->getPersonaById($idPersona);
		$data['domicilio'] = $this->domicilio_model->loadDomicilio();
		$this->load->view('domicilio_view',$data);
	}
	public function index()
	{
		$this->validAccess();
		$data['session'] = $this->session->userdata('up_session');
		$data['domicilio'] = $this->domicilio_model->loadDomicilio();
		$this->load->view('domicilio_view',$data);
	}

	public function saveDomicilio()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('calleDomicilio','Calle','required|max_length[30]');
		$this->form_validation->set_rules('noIntDomicilio','Numero interior','max_length[5]');
		$this->form_validation->set_rules('noExtDomicilio','Numero exterior','required|max_length[5]');
		$this->form_validation->set_rules('colonia','Colonia','required|max_length[20]');
		$this->form_validation->set_rules('municipio','Municipio','required|max_length[35]');
		$this->form_validation->set_rules('estado','Estado','required|max_length[35]');
		$this->form_validation->set_rules('personaDomiclio','Persona','required');
		if($this->form_validation->run()==FALSE)
		{
			$data['domicilio'] = $this->domicilio_model->loadDomicilio();
			$this->session->set_flashdata('domicilioSaveError','Error saving information, valid fields');

			$data['domicilio'] = $this->domicilio_model->loadDomicilio();
			$this->load->view('domicilio_view',$data);

		}
		else{
			$data = array(
			"calleDomicilio"=>$this->input->post('calleDomicilio'),
			"noIntDomicilio"=>$this->input->post('noIntDomicilio'),
			"noExtDomicilio"=>$this->input->post('noExtDomicilio'),
			"colonia"=>$this->input->post('colonia'),
			"municipio"=>$this->input->post('municipio'),
			"estado"=>$this->input->post('estado'),
			"fkPersona"=>$this->input->post('personaDomiclio'),
			);
			if($this->input->post('idDomicilio'))
			{
				
				$idDomicilio = $this->input->post('idDomicilio');

				$this->domicilio_model->editDomicilio($data,$idDomicilio);
				$this->session->set_flashdata('domicilioSaveSuccess','Information edited successfully');
				redirect('Domicilio');
			}
			else
			{
				$this->domicilio_model->saveDomicilio($data);
				$direccion = $this->domicilio_model->loadLastAddress();
				$this->domicilio_model->asignfk($this->input->post('personaEmpleado'),$direccion->idDomicilio);
				$this->session->set_flashdata('domicilioSaveSuccess','Information saved successfully');
				redirect('Domicilio');
			}
		}
	}

	public function editDomicilio($idDomicilio=NULL){
		if($idDomicilio!=NULL){
			$domicilioToEdit = $this->domicilio_model->getDomicilioById($idDomicilio);
			if($domicilioToEdit==NULL){
				$this->session->set_flashdata('domicilioSaveError','Event not found');
				redirect('Domicilio');
			}else{
				$data['persona'] = $this->domicilio_model->getPersonaById($domicilioToEdit->fkPersona);
				$data['domicilio'] = $this->domicilio_model->loadDomicilio();
				$data['domicilioToEdit'] = $domicilioToEdit;
				$this->load->view('domicilio_view',$data);
			}
			
		}else{
			$this->session->set_flashdata('domicilioSaveError','Event not found');
			redirect('domicilio_view');
		}
	}

	function validAccess() {
		$s =$this->session->userdata('up_session');
		if (!@$s['email'] || !@$s['isLogged']) {
			redirect('Login');
		}
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    
    public function __construct(){
        parent::__construct();
        $this->load->model('Auth_model');
    }



    public function index(){
    
        $this->load->view('authView');
    }
   public function authenticate(){
        $isValid = $this->Auth_model->authenticate($this->input->post('correoUsuario'),$this->input->post('contraseñaUsuario'));
        if($isValid){
            $session_data = array(
                'email' => $isValid->correoUsuario,
                'isLogged' =>TRUE
                );
            $this->session->set_userdata('up_session',$session_data);
            $this->session->set_flashdata('LoginSuccess','Welcome'.$isValid->correoUsuario);
            redirect('Cargo');
        }else{
            $this->session->set_flashdata('LoginError','Username or Password incorrect, try again!');
            
            redirect('Login');
        }
        
    }
    public function closeSession(){
        $this->session->sess_destroy();
        redirect('Login');
    }

  } 

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carrera extends CI_Controller 
{
	public function __construct(){
		parent::__construct();
		$this->load->model('Carreras_model');
	}

	public function index()
	{
		$data['carreras'] = $this->Carreras_model->loadCarreras();
		$data['session'] = $this->session->userdata('up_session');
		$this->load->view('carreras',$data);
	}

	public function saveCarreras()
	{
		$this->form_validation->set_rules('nombreCarrera','Nombre','required|max_length[50]');

		$this->form_validation->set_rules('ClaveCarrera','Clave','required|max_length[18]');		

		if($this->form_validation->run()==FALSE)
		{
			$data['carreras'] = $this->Carreras_model->loadCarreras();
			$this->session->set_flashdata('carreraSaveError','Error saving information, valid fields');

			$data['carreras'] = $this->Carreras_model->loadCarreras();
			$this->load->view('carreras',$data);

		}
		else{
			$data = array(
			"nombreCarrera"=>$this->input->post('nombreCarrera'),
			"ClaveCarrera"=>$this->input->post('ClaveCarrera'),
			"EstatusCarrera"=>"Activa",
			);

			if($this->input->post('idCarrera'))
			{
				
				$idCarrera = $this->input->post('idCarrera');

				$this->Carreras_model->editCarreras($data,$idCarrera);
				$this->session->set_flashdata('carreraSaveSuccess','Information edited successfully');
				redirect('Carrera');
			}
			else
			{
				$this->Carreras_model->saveCarreras($data);
				$this->session->set_flashdata('carreraSaveSuccess','Information saved successfully');
				redirect('Carrera');
			}
		}
	}

	public function editCarreras($idCarrera=NULL){
		if($idCarrera!=NULL){
			$carreraToEdit = $this->Carreras_model->getCarrerasById($idCarrera);
			if($carreraToEdit==NULL){
				$this->session->set_flashdata('carreraSaveError','Event not found');
				redirect('Carrera');
			}else{
				$data['carreras'] = $this->Carreras_model->loadCarreras();
				$data['carreraToEdit'] = $carreraToEdit;
				$this->load->view('carreras',$data);
			}
			
		}else{
			$this->session->set_flashdata('carreraSaveError','Event not found');
			redirect('Carrera');
		}
	}

	function editStatusCarrera($idCarrera=NULL){
		if ($idCarrera!=null){
			$carreraToEdit = $this->Carreras_model->loadCarrerasById($idCarrera);
			if ($carreraToEdit==NULL){
				$this->session->set_flashdata('carreraSaveError','Event not found');
				redirect('Carrera');
			} else{
				if($carreraToEdit->EstatusCarrera=="Activa"){
					$data = array(
						"EstatusCarrera"  =>"Inactiva"
					);
				} else if($carreraToEdit->EstatusCarrera=="Inactiva"){
					$data = array(
						"EstatusCarrera" =>"Activa"
					);
				}

				$this->Carreras_model->editEstatus($data,$idCarrera);
				$this->session->set_flashdata('carreraSaveSuccess','Information saved successfully');
				redirect('Carrera');
			}
		} else{
			$this->session->set_flashdata('carreraSaveError','Event not found');
			redirect('Carrera');
		}
	}
}

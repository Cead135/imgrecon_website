<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empleado extends CI_Controller 
{
	public function __construct(){
		parent::__construct();
		$this->load->model('empleado_model');
	}
	public function addInfoNew()
	{
		$data['session'] = $this->session->userdata('up_session');
		$this->validAccess();
		$data['empleados'] = $this->empleado_model->loadEmpleado();
		$data['areas'] = $this->empleado_model->loadAreas();
		$data['cargos'] = $this->empleado_model->loadCargos();
		$data['carreras'] = $this->empleado_model->loadCarreras();
		$data['grupos'] = $this->empleado_model->loadGrupos();
		$data['tipoId'] = $this->empleado_model->loadIdentificaciones();
		$data['domicilio'] = $this->empleado_model->loadDomicilio();
		$data['persona'] = $this->empleado_model->loadLastPersona();

		$this->load->view('empleado_view',$data);
	}
	public function addInfo($idPersona=NULL)
	{
		$data['session'] = $this->session->userdata('up_session');
		$this->validAccess();
		$data['persona'] = $this->empleado_model->getPersonaById($idPersona);
		$data['empleados'] = $this->empleado_model->loadEmpleado();
		$data['areas'] = $this->empleado_model->loadAreas();
		$data['cargos'] = $this->empleado_model->loadCargos();
		$data['carreras'] = $this->empleado_model->loadCarreras();
		$data['grupos'] = $this->empleado_model->loadGrupos();
		$data['tipoId'] = $this->empleado_model->loadIdentificaciones();
		$data['domicilio'] = $this->empleado_model->loadDomicilio();
		$this->load->view('empleado_view',$data);

	}
	public function index()
	{
		$data['session'] = $this->session->userdata('up_session');
		$this->validAccess();
		$data['areas'] = $this->empleado_model->loadAreas();
		$data['empleados'] = $this->empleado_model->loadEmpleado();
		$data['carreras'] = $this->empleado_model->loadCarreras();
		$data['grupos'] = $this->empleado_model->loadGrupos();
		$data['tipoId'] = $this->empleado_model->loadIdentificaciones();
		$data['domicilio'] = $this->empleado_model->loadDomicilio();
		$data['cargos'] = $this->empleado_model->loadCargos();

		$this->load->view('empleado_view',$data);
	}

	public function saveEmpleado()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nombreEmpleado','Nombre','required|max_length[50]|min_length[4]');
		$this->form_validation->set_rules('apellidosEmpleado','Apellido','required|max_length[50]|min_length[4]');
		$this->form_validation->set_rules('fechaNacEmpleado','Fecha de nacimiento','required');
		$this->form_validation->set_rules('generoEmpleado','Genero','required');
		$this->form_validation->set_rules('curpEmpleado','CURP','required|max_length[30]|min_length[4]');
		$this->form_validation->set_rules('rfcEmpleado','RFC','required|max_length[50]|min_length[12]');
		$this->form_validation->set_rules('areaEmpleado','Area','required');
		$this->form_validation->set_rules('carreraEmpleado','Carrera');
		$this->form_validation->set_rules('tipoIdEmpleado','Tipo de identificacion','required');
		$this->form_validation->set_rules('codigoIdentificacion','Codigo','required|max_length[50]|min_length[4]');
		$this->form_validation->set_rules('codigoEmpleado','Matricula','required|max_length[20]|min_length[4]');
		$this->form_validation->set_rules('cargoEmpleado','Cargo','required');
		$this->form_validation->set_rules('personaEmpleado','Persona','required');
		if($this->form_validation->run()==FALSE)
		{
			$data['empleados'] = $this->empleado_model->loadEmpleado();
			$this->session->set_flashdata('empleadoSaveError','Error saving information, valid fields');

			$data['empleados'] = $this->empleado_model->loadEmpleado();
			$this->load->view('empleado_view',$data);

		}
		else{
			$data = array(
			"nombreEmpleado"=>$this->input->post('nombreEmpleado'),
			"apellidosEmpleado"=>$this->input->post('apellidosEmpleado'),
			"fechaNacEmpleado"=>$this->input->post('fechaNacEmpleado'),
			"generoEmpleado"=>$this->input->post('generoEmpleado'),
			"curpEmpleado"=>$this->input->post('curpEmpleado'),
			"RFC"=>$this->input->post('rfcEmpleado'),
			"fkareaEmpleado"=>$this->input->post('areaEmpleado'),
			"fkcarreraEmpleado"=>$this->input->post('carreraEmpleado'),
			"fktipoIdentificacion"=>$this->input->post('tipoIdEmpleado'),
			"codigoIdentificacion"=>$this->input->post('codigoIdentificacion'),
			"codigoEmpleado"=>$this->input->post('codigoEmpleado'),
			"fkCargoEmpleado"=>$this->input->post('cargoEmpleado'),
			"fkPersonaEmpleado"=>$this->input->post('personaEmpleado'),
			);

			if($this->input->post('idEmpleado'))
			{
				
				$idEmpleado = $this->input->post('idEmpleado');

				$this->empleado_model->editEmpleado($data,$idEmpleado);
				$this->session->set_flashdata('empleadoSaveSuccess','Information edited successfully');
				redirect('Empleado');
			}
			else
			{
				$this->empleado_model->saveEmpleado($data);
				$this->session->set_flashdata('empleadoSaveSuccess','Information saved successfully');
				redirect('Empleado');
			}
		}
	}

	public function editEmpleados($idEmpleado=NULL){
		if($idEmpleado!=NULL){
			$empleadoToEdit = $this->empleado_model->getEmpleadoById($idEmpleado);
			if($empleadoToEdit==NULL){
				$this->session->set_flashdata('empleadoSaveError','Event not found');
				redirect('empleado_view');
			}else{
				$data['empleados'] = $this->empleado_model->loadEmpleado();
				$data['carreras'] = $this->empleado_model->loadCarreras();
				$data['grupos'] = $this->empleado_model->loadGrupos();
				$data['tipoId'] = $this->empleado_model->loadIdentificaciones();
				$data['empleadoToEdit'] = $empleadoToEdit;
				$this->load->view('empleado_view',$data);
			}
			
		}else{
			$this->session->set_flashdata('empleadoSaveError','Event not found');
			redirect('empleado_view');
		}
	}

	function editStatusEmpleados($idEmpleado=NULL){
		if ($idEmpleado!=null){
			$empleadoToEdit = $this->empleado_model->loadTarjeById($idEmpleado);
			if ($empleadoToEdit==NULL){
				$this->session->set_flashdata('empleadoSaveError','Event not found');
				redirect('Empleado');
			} else{
				if($empleadoToEdit->statusEmpleado=="Activo"){
					$data = array(
						"statusEmpleado"  =>"Inactivo"
					);
				} else if($empleadoToEdit->statusEmpleado=="Inactivo"){
					$data = array(
						"statusEmpleado" =>"Activo"
					);
				}

				$this->empleado_model->editEstatus($data,$idEmpleado);
				$this->session->set_flashdata('empleadoSaveSuccess','Information saved successfully');
				redirect('Empleado');
			}
		} else{
			$this->session->set_flashdata('empleadoSaveError','Event not found');
			redirect('Empleado');
		}
	}
	function validAccess() {
		$s =$this->session->userdata('up_session');
		if (!@$s['email'] || !@$s['isLogged']) {
			redirect('Login');
		}
	}
}
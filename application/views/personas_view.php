
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ImgRecon</title>
    <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/datepicker3.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/styles.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" ><span>Img</span>Recon</a>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <em class="fa fa-envelope"></em>
                            <span class="label label-danger"></span>
                        </a>

                        <ul class="dropdown-menu dropdown-messages">
                            <li>
                                <div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="<?php echo base_url();?>/placehold.it/40/30a5ff/fff">
                                    </a>
                                    <div class="message-body"><small class="pull-right">3 mins ago</small>
                                        <a href="#"><strong>John Doe</strong> commented on <strong>your photo</strong>.</a>
                                    <br /><small class="text-muted">1:24 pm - 25/03/2015</small></div>
                                </div>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="<?php echo base_url();?>/placehold.it/40/30a5ff/fff">
                                    </a>
                                    <div class="message-body"><small class="pull-right">1 hour ago</small>
                                        <a href="#">New message from <strong>Jane Doe</strong>.</a>
                                    <br /><small class="text-muted">12:27 pm - 25/03/2015</small></div>
                                </div>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="all-button"><a href="#">
                                    <em class="fa fa-inbox"></em> <strong>All Messages</strong>
                                </a></div>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <em class="fa fa-bell"></em><span class="label label-info"></span>
                    </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li><a href="#">
                                <div><em class="fa fa-envelope"></em> 1 New Message
                                    <span class="pull-right text-muted small"></span></div>
                            </a></li>
                        </ul>
                    </li>

                    <li class="dropdown"><a href="login.html" class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <em class="fa fa-power-off"></em><span class="label label-info"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                            <li><a href="#">
                                <div><em class="fa fa-edit"></em> EDITAR PERFIL
                                    <span class="pull-right text-muted small"></span></div>
                            </a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo base_url().'index.php/Login/closeSession';?>">
                                <div><em class="fa fa-user"></em> CERRAR SESIÓN<font color="#286bcc"><?php echo $session['email'];?></font>
                                    <span class="pull-right text-muted small"></span></div>
                            </a></li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
    </nav>

    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar"> 
    <ul class="nav menu">           
        <li><a href="<?php echo base_url().'index.php/Cargo';?>"><em class="fa fa-clone">&nbsp;</em> Cargos </a></li>
        <li><a href="<?php echo base_url().'index.php/Area';?>"><em class="fa fa-clone">&nbsp;</em> Areas </a></li>    
        <li><a href="<?php echo base_url().'index.php/Carrera';?>"><em class="fa fa-clone">&nbsp;</em> Carreras </a></li>
        <li><a href="<?php echo base_url().'index.php/Grupo';?>"><em class="fa fa-clone">&nbsp;</em> Grupos </a></li>
        <li><a href="<?php echo base_url().'index.php/Tarjeton';?>"><em class="fa fa-clone">&nbsp;</em> Tarjetones </a></li>
        <li><a href="<?php echo base_url().'index.php/Tipoiden';?>"><em class="fa fa-clone">&nbsp;</em> Identificaciones </a></li>
        <li><a href="<?php echo base_url().'index.php/Domicilio';?>"><em class="fa fa-clone">&nbsp;</em> Domicilio </a></li>
        <li><a href="<?php echo base_url().'index.php/personas_control';?>"><em class="fa fa-clone">&nbsp;</em> Personas </a></li>
        <li><a href="<?php echo base_url().'index.php/Alumno';?>"><em class="fa fa-clone">&nbsp;</em> Alumnos </a></li>
        <li><a href="<?php echo base_url().'index.php/Empleado';?>"><em class="fa fa-clone">&nbsp;</em> Empleados </a></li>
        <li><a href="<?php echo base_url().'index.php/Visitante';?>"><em class="fa fa-clone">&nbsp;</em> Visitantes </a></li>
    </ul>
    </div>
        
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li class="active">UPSRJ SECURITY</li>
            </ol>
        </div>        
         <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Personas</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">Nueva Persona</div>
                        <div class="panel-body">
                           <?php if($this->session->flashdata('gameSaveError')!=null){ ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo $this->session->flashdata('gameSaveError');?>
                                </div>

                            <?php } ?>
                            <?php if($this->session->flashdata('gameSaveSuccess')!=null){ ?>
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo $this->session->flashdata('gameSaveSuccess');?>
                                </div>
                            <?php } ?>
                            
                           <?php echo form_open('personas_control/savePersonas')?>
                                
                            
                             <?php if(isset($personaToEdit)){?>
                                    <input type="hidden" name="idPersona" value="<?php echo $personaToEdit->idPersona?>">
                                <?php } ?>
                                <div class="col-xs-6">
                                <?php if(form_error('nombrePersona')!=null){?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('nombrePersona');?>
                                    </div>
                                <?php } ?>
                            
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" value="<?php if(isset($personaToEdit)){ echo $personaToEdit->nombrePersona;} ?>" name="nombrePersona" id="nombrePersona" class="form-control">
                                    <p class="help-block">Ex. Willy</p>
                                </div>
                            </div>
                            <div class="col-xs-6">
                             <?php if(form_error('apellidosPersona')!=null){?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('apellidosPersona');?>
                                    </div>
                                <?php } ?>
                            
                                  <div class="form-group">
                                    <label>Apellidos</label>
                                    <input type="text" value="<?php if(isset($personaToEdit)){ echo $personaToEdit->apellidosPersona;} ?>" name="apellidosPersona" id="apellidosPersona" class="form-control">
                                    <p class="help-block">Ex. Miranda</p>
                                </div>
                            </div>
                                
                                    <div class="col-xs-6">
                                        <?php if(form_error('correoPersona')!=null){?>
                                            <div class="alert alert-danger alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                <?php echo form_error('correoPersona');?>
                                            </div>
                                        <?php } ?>
                               
                                <div class="form-group">
                                    <label>E-mail</label>
                                    <input type="text" value="<?php if(isset($personaToEdit)){ echo $personaToEdit->correoPersona;} ?>" name="correoPersona" id="correoPersona" class="form-control">
                                    <p class="help-block">Ex. WillyM@gmail.com</p>
                                </div>
                                </div>
                                 <div class="col-xs-6">
                                <?php if(form_error('generoPersona')!=null){?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('generoPersona');?>
                                    </div>
                                <?php } ?>
                               
                            <div class="form-group">
                                <label>Genéro</label>
                                <select name="generoPersona" id="generoPersona" class="form-control">

                                <option value=""> Seleccione una opción</option>
                                <option value="Hombre" <?php if(isset($personaToEdit)){if($personaToEdit->generoPersona=="Hombre"){echo "selected";}}?>>Hombre</option>
                                 <option  value="Mujer" <?php if(isset($personaToEdit)){if($personaToEdit->generoPersona=="Mujer"){echo "selected";}}?>>Mujer</option>             
                                </select>
                            </div>
                            </div>

                            <div class="col-xs-6">
                                <?php if(form_error('tipoPersona')!=null){?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('tipoPersona');?>
                                    </div>
                                <?php } ?>
                               
                            <div class="form-group">
                                <label>Tipo</label>
                                <select name="tipoPersona" id="tipoPersona" class="form-control">
                                    <option value=""> Seleccione una opción</option>
                                    <option value="Alumno">Alumno</option>
                                    <option value="Empleado">Empleado</option>
                                    <option value="Visitante">Visitante</option>        
                                </select>
                            </div>
                            </div>
                                    
                                 <div class="col-xs-8">
                                <button type="reset" class="btn btn-danger">Cancel</button>
                                </div>
                                <div class="col-xs-2">
                                <button type="submit" class="btn btn-success">Save</button>
                                </div>
                                 </div>
                            <?php echo form_close();?> 
                        </div>
                    </div>
                </div>
            </div>
             <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped table-bordered table-hover" id="gameTable"> 
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Apellidos</th>
                                <th>E-mail</th>
                                <th>Genéro</th>
                                <th>Editar</th>
                                <th>Agregar Inf.</th>
                                <th>Agregar domicilio</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($persona as $iPersona){?>
                            <tr>
                                <td><?php echo $iPersona->nombrePersona;?></td>
                                <td><?php echo $iPersona->apellidosPersona;?></td>
                                <td><?php echo $iPersona->correoPersona;?></td>
                                <td><?php echo $iPersona->generoPersona;?></td>
                                <td><?php echo $iPersona->generoPersona;?></td>
                                <td>
                                     <a href="<?php echo base_url().'index.php/personas_control/editPersonas/'.$iPersona->idPersona;?>" class="btn btn-success"><i class="fa fa-edit"></i></a>
                                </td>
                                <td>
                                    <a href="<?php echo base_url().'index.php/personas_control/addInfo/'.$iPersona->idPersona;?>" class="btn btn-danger"><i class="fa fa-edit"></i></a>
                                </td>
                                <td>
                                    <a href="<?php echo base_url().'index.php/personas_control/addAddress/'.$iPersona->idPersona;?>" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                </td>
                                 
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        
        
        
    </div>  <!--/.main-->
    
    <script src="<?php echo base_url();?>js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/chart.min.js"></script>
    <script src="<?php echo base_url();?>js/chart-data.js"></script>
    <script src="<?php echo base_url();?>js/easypiechart.js"></script>
    <script src="<?php echo base_url();?>js/easypiechart-data.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>js/custom.js"></script>
</body>
</html>
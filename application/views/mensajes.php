<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ImgRecon</title>
    <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/datepicker3.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/styles.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" ><span>Img</span>Recon</a>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <em class="fa fa-envelope"></em>
                            <span class="label label-danger"></span>
                        </a>

                        <ul class="dropdown-menu dropdown-messages">
                            <li>
                                <div class="dropdown-messages-box"><a href="" class="pull-left">
                                   
                                    </a>
                                    
                                </div>
                            </li>
                           
                            <li>
                                <div class="all-button"><a href="<?php echo base_url().'index.php/Mensajenoleidos';?>">
                                    <em class="fa fa-inbox"></em> <strong>Bandeja</strong>
                                </a></div>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <em class="fa fa-bell"></em><span class="label label-info"></span>
                    </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li><a href="#">
                                <div><em class="fa fa-envelope"></em> 1 New Message
                                    <span class="pull-right text-muted small"></span></div>
                            </a></li>
                        </ul>
                    </li>
                    

                    <li class="dropdown"><a href="login.html" class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <em class="fa fa-power-off"></em><span class="label label-info"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                            <li><a href="#">
                                <div><em class="fa fa-edit"></em> EDITAR PERFIL
                                    <span class="pull-right text-muted small"></span></div>
                            </a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo base_url().'index.php/Login/closeSession';?>">
                                <div><em class="fa fa-user"></em> CERRAR SESIÓN 
                                    <font color="#286bcc">
                                        <?php echo $session['email'];?>
                                    </font>
                                    <span class="pull-right text-muted small"></span></div>
                            </a></li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
    </nav>

    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar"> 
    <ul class="nav menu">           
        <li><a href="<?php echo base_url().'index.php/Cargo';?>"><em class="fa fa-clone">&nbsp;</em> Cargos </a></li>
        <li><a href="<?php echo base_url().'index.php/Area';?>"><em class="fa fa-clone">&nbsp;</em> Areas </a></li>    
        <li><a href="<?php echo base_url().'index.php/Carrera';?>"><em class="fa fa-clone">&nbsp;</em> Carreras </a></li>
        <li><a href="<?php echo base_url().'index.php/Grupo';?>"><em class="fa fa-clone">&nbsp;</em> Grupos </a></li>
        <li><a href="<?php echo base_url().'index.php/Tarjeton';?>"><em class="fa fa-clone">&nbsp;</em> Tarjetones </a></li>
        <li><a href="<?php echo base_url().'index.php/Tipoiden';?>"><em class="fa fa-clone">&nbsp;</em> Identificaciones </a></li>
        <li><a href="<?php echo base_url().'index.php/Domicilio';?>"><em class="fa fa-clone">&nbsp;</em> Domicilio </a></li>
        <li><a href="<?php echo base_url().'index.php/personas_control';?>"><em class="fa fa-clone">&nbsp;</em> Personas </a></li>
        <li><a href="<?php echo base_url().'index.php/Alumno';?>"><em class="fa fa-clone">&nbsp;</em> Alumnos </a></li>
        <li><a href="<?php echo base_url().'index.php/Empleado';?>"><em class="fa fa-clone">&nbsp;</em> Empleados </a></li>
        <li><a href="<?php echo base_url().'index.php/Visitante';?>"><em class="fa fa-clone">&nbsp;</em> Visitantes </a></li>
    </ul>
    </div>
        
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li class="active">UPSRJ SECURITY</li>
            </ol>
        </div>
        
         <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Todos los mensajes</h1>
                </div>
            </div>
            
             <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">NUEVO MENSAJE</div>
                        <div class="panel-body">

                        <?php echo validation_errors();?>
                        
                        <?php if($this->session->flashdata('mensajeSaveSuccess')!=null){ ?>
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo $this->session->flashdata('mensajeSaveSuccess');?>
                                </div>
                        <?php } ?>

                        <?php echo form_open_multipart('mensaje/saveMensajes',array('id'=>"mensajesForm"))?>
                            
                            <?php if(isset($mensajeToEdit)){?>
                                <input type="hidden" name="idMensaje" value="<?php echo $mensajeToEdit->idMensaje?>">
                            <?php } ?>

                                <div class="col-xs-6">
                                <?php if(form_error('fkPersonaMensaje')!=null){?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                            <?php echo form_error('fkPersonaMensaje');?>
                                    </div>
                                <?php } ?>

                                <div class="form-group">
                                    <label>MENSAJE PARA:</label>
                                    <select name="fkPersonaMensaje" class="form-control">

                                    <?php foreach($personas_control as $iGame){?>
                                        <option value="<?php echo $iGame->idPersona;?>" <?php if(isset($mensajeToEdit) && $mensajeToEdit->fkPersonaMensaje==$iGame->idPersona){echo "selected"; }?> ><?php echo $iGame->nombrePersona;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                </div>

                                <div class="col-xs-6">
                                    <?php if(form_error('tipoMensaje')!=null){?>
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                <?php echo form_error('tipoMensaje');?>
                                        </div>
                                    <?php } ?>

                                <div class="form-group">
                                    <label>TIPO DE MENSAJE</label>
                                        <select id="tipoMensaje" name="tipoMensaje" class="form-control">
                                            <option value="">
                                                Seleccionar
                                            </option>
                                                <option value="Normal" <?php if(isset($mensajeToEdit)){if($mensajeToEdit->tipoMensaje=="Normal"){echo "selected";}}?>>
                                                    Normal
                                                </option value="Sin importancia" <?php if(isset($mensajeToEdit)){if($mensajeToEdit->tipoMensaje=="Sin importancia"){echo "selected";}}?>>
                                                <option>
                                                    Sin importancia
                                                </option>
                                                <option value="Urgente" <?php if(isset($mensajeToEdit)){if($mensajeToEdit->tipoMensaje=="Urgente"){echo "selected";}}?>>
                                                    Urgente
                                                </option>
                                        </select>
                                </div>
                                </div>

                                <div class="col-xs-12">
                                    <?php if(form_error('tituloMensaje')!=null){?>
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                <?php echo form_error('tituloMensaje');?>
                                        </div>
                                    <?php } ?>

                                <div class="form-group">
                                    <label>TITULO DEL MENSAJE</label>
                                        <input type="text" name="tituloMensaje" id="tituloMensaje" class="form-control" value="<?php if(isset($mensajeToEdit)){ echo $mensajeToEdit->tituloMensaje;}?>">
                                            <p class="help-block">Ejemplo: Cambio de estacionamiento</p>
                                </div>
                                </div>       

                                <div class="col-xs-12">
                                        <?php if(form_error('motivoMensaje')!=null){?>
                                            <div class="alert alert-danger alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                    <?php echo form_error('motivoMensaje');?>
                                            </div>
                                        <?php } ?>

                                            <div class="form-group">
                                                <label>DESCRIPCION</label>
                                                <textarea class="form-control" name="motivoMensaje" id="motivoMensaje" rows="5"><?php if(isset($mensajeToEdit)){ echo $mensajeToEdit->motivoMensaje;}?></textarea>
                                                <p class="help-block">Ejemplo: El estacionamiento por el momento se escuentra...</p>
                                            </div>
                                    </div>

                                <div class="col-xs-8">
                                    <button type="reset" class="btn btn-danger">Cancelar</button>                             
                                </div>
                                <div class="col-xs-2">   
                                    <button type="submit" class="btn btn-success">Guardar</button>    
                                </div>
                            <?php echo form_close();?>        
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped table-bordered table-hover" id="cargosTable"> 
                        <thead>
                            <tr>
                                <th>Mensaje para:</th>
                                <th>Tipo de mensaje</th>
                                <th>Fecha de enviado</th>
                                <th>Fue entregado</th>
                                <th>Titulo</th>
                                <th>Descripcion</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($mensajes as $iMensa){?>
                            <tr>
                        
                                <td><?php echo $iMensa->nombrePersona; ?></td>
                                <td><?php echo $iMensa->tipoMensaje; ?></td>
                                <td><?php echo $iMensa->fechaGeneracionMensaje; ?></td>
                                <td><?php echo $iMensa->fueEntregadoMensaje; ?></td>
                                <td><?php echo $iMensa->tituloMensaje; ?></td>
                                <td><?php echo $iMensa->motivoMensaje; ?></td>
                                <td>
                                    <a href="<?php echo base_url().'index.php/mensaje/editMensajes/'.$iMensa->idMensaje;?>" class="btn btn-success"><i class="fa fa-edit"></i></a>   
                                    <a href="<?php echo base_url().'index.php/mensaje/editStatusMensaje/'.$iMensa->idMensaje;?>" class="btn btn-danger"><i class="fa fa-share"></i></a>
                                </td>
                        
                            </tr>
                            <?php } ?>
                        </tbody>                        
                    </table>
                </div>
            </div>
        </div>

    </div>
    <script src="<?php echo base_url();?>js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/chart.min.js"></script>
    <script src="<?php echo base_url();?>js/chart-data.js"></script>
    <script src="<?php echo base_url();?>js/easypiechart.js"></script>
    <script src="<?php echo base_url();?>js/easypiechart-data.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>js/custom.js"></script>       
</body>
</html>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ImgRecon</title>
    <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/datepicker3.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/styles.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" ><span>Img</span>Recon</a>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <em class="fa fa-envelope"></em>
                            <span class="label label-danger"></span>
                        </a>

                        <ul class="dropdown-menu dropdown-messages">
                            <li>
                                <div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="<?php echo base_url();?>/placehold.it/40/30a5ff/fff">
                                    </a>
                                    <div class="message-body"><small class="pull-right">3 mins ago</small>
                                        <a href="#"><strong>John Doe</strong> commented on <strong>your photo</strong>.</a>
                                    <br /><small class="text-muted">1:24 pm - 25/03/2015</small></div>
                                </div>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="<?php echo base_url();?>/placehold.it/40/30a5ff/fff">
                                    </a>
                                    <div class="message-body"><small class="pull-right">1 hour ago</small>
                                        <a href="#">New message from <strong>Jane Doe</strong>.</a>
                                    <br /><small class="text-muted">12:27 pm - 25/03/2015</small></div>
                                </div>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="all-button"><a href="#">
                                    <em class="fa fa-inbox"></em> <strong>All Messages</strong>
                                </a></div>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <em class="fa fa-bell"></em><span class="label label-info"></span>
                    </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li><a href="#">
                                <div><em class="fa fa-envelope"></em> 1 New Message
                                    <span class="pull-right text-muted small"></span></div>
                            </a></li>
                        </ul>
                    </li>

                    <li class="dropdown"><a href="login.html" class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <em class="fa fa-power-off"></em><span class="label label-info"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                            <li><a href="#">
                                <div><em class="fa fa-edit"></em> EDITAR PERFIL
                                    <span class="pull-right text-muted small"></span></div>
                            </a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo base_url().'index.php/Login/closeSession';?>">
                                <div><em class="fa fa-user"></em> CERRAR SESIÓN<font color="#286bcc"><?php echo $session['email'];?></font>
                                    <span class="pull-right text-muted small"></span></div>
                            </a></li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
    </nav>

    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar"> 
    <ul class="nav menu">           
        <li><a href="<?php echo base_url().'index.php/Cargo';?>"><em class="fa fa-clone">&nbsp;</em> Cargos </a></li>
        <li><a href="<?php echo base_url().'index.php/Area';?>"><em class="fa fa-clone">&nbsp;</em> Areas </a></li>    
        <li><a href="<?php echo base_url().'index.php/Carrera';?>"><em class="fa fa-clone">&nbsp;</em> Carreras </a></li>
        <li><a href="<?php echo base_url().'index.php/Grupo';?>"><em class="fa fa-clone">&nbsp;</em> Grupos </a></li>
        <li><a href="<?php echo base_url().'index.php/Tarjeton';?>"><em class="fa fa-clone">&nbsp;</em> Tarjetones </a></li>
        <li><a href="<?php echo base_url().'index.php/Tipoiden';?>"><em class="fa fa-clone">&nbsp;</em> Identificaciones </a></li>
        <li><a href="<?php echo base_url().'index.php/Domicilio';?>"><em class="fa fa-clone">&nbsp;</em> Domicilio </a></li>
        <li><a href="<?php echo base_url().'index.php/personas_control';?>"><em class="fa fa-clone">&nbsp;</em> Personas </a></li>
        <li><a href="<?php echo base_url().'index.php/Alumno';?>"><em class="fa fa-clone">&nbsp;</em> Alumnos </a></li>
        <li><a href="<?php echo base_url().'index.php/Empleado';?>"><em class="fa fa-clone">&nbsp;</em> Empleados </a></li>
        <li><a href="<?php echo base_url().'index.php/Visitante';?>"><em class="fa fa-clone">&nbsp;</em> Visitantes </a></li>
    </ul>
    </div>
        
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li class="active">UPSRJ SECURITY</li>
            </ol>
        </div>        
<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Tarjetones</h1>
    </div>
</div>

 <div class="row">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Nuevo tarjeton</div>
            <div class="panel-body">

            <?php echo validation_errors();?>
            
            <?php if($this->session->flashdata('tarjetonSaveSuccess')!=null){ ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('tarjetonSaveSuccess');?>
                    </div>
            <?php } ?>


            <?php echo form_open_multipart('Tarjeton/saveTarjeton',array('id'=>"tarjetonForm"))?>
                
                <?php if(isset($tarjetonToEdit)){?>
                    <input type="hidden" name="idTarjeton" value="<?php echo $tarjetonToEdit->idTarjeton?>">
                <?php } ?>

                
                       
                    <div class="col-xs-12">
                        <?php if(form_error('vigenciaTarjeton')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('vigenciaTarjeton');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>Vigencia</label>
                            <input type="date" name="vigenciaTarjeton" id="vigenciaTarjeton" class="form-control" value="<?php if(isset($tarjetonToEdit)){ echo $tarjetonToEdit->vigenciaTarjeton;}?>">
                                <p class="help-block">Ejemplo: 15/11/2020</p>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <?php if(form_error('noTarjeton')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('noTarjeton');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>No. de tarjeton</label>
                            <input type="text" name="noTarjeton" id="noTarjeton" class="form-control" value="<?php if(isset($tarjetonToEdit)){ echo $tarjetonToEdit->noTarjeton;}?>">
                                <p class="help-block">Ejemplo: A6841</p>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <?php if(form_error('tipoTarjeton')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('tipoTarjeton');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>Tipo</label>
                        <select id="tipoTarjeton" name="tipoTarjeton" class="form-control">
                            <option value="">
                                Seleccionar
                            </option>
                            <option value="Estudiante" <?php if(isset($tarjetonToEdit)){if($tarjetonToEdit->tipoTarjeton=="Estudiante"){echo "selected";}}?>>
                                Estudiante
                            </option value="Empleado" <?php if(isset($tarjetonToEdit)){if($tarjetonToEdit->tipoTarjeton=="Empleado"){echo "selected";}}?>>
                            <option>
                                Empleado
                            </option>
                            <option value="Visitante" <?php if(isset($tarjetonToEdit)){if($tarjetonToEdit->tipoTarjeton=="Visitante"){echo "selected";}}?>>
                                Visitante
                            </option>
                        </select>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <?php if(form_error('colorTarjeton')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('colorTarjeton');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>Color</label>
                        <select id="colorTarjeton" name="colorTarjeton" class="form-control">
                            <option value="">
                                Seleccionar
                            </option>
                            <option value="Morado" <?php if(isset($tarjetonToEdit)){if($tarjetonToEdit->colorTarjeton=="Morado"){echo "selected";}}?>>
                                Morado
                            </option value="Naranja" <?php if(isset($tarjetonToEdit)){if($tarjetonToEdit->colorTarjeton=="Naranja"){echo "selected";}}?>>
                            <option>
                                Naranja
                            </option>
                            <option value="Verde" <?php if(isset($tarjetonToEdit)){if($tarjetonToEdit->colorTarjeton=="Verde"){echo "selected";}}?>>
                                Verde
                            </option>
                        </select>
                        </div>
                    </div>
                                       
                    <div class="col-xs-8">
                        <button type="reset" class="btn btn-danger">Cancelar</button>                             
                    </div>
                    <div class="col-xs-2">   
                        <button type="submit" class="btn btn-success">Guardar</button>    
                    </div>
                <?php echo form_close();?>        
            </div>
        </div>
    </div>
</div>


            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped table-bordered table-hover" id="tarjetonesTable"> 
                        <thead>
                            <tr>
                                <th>Vigencia</th>
                                <th>Estado</th>
                                <th>Numero</th>
                                <th>Tipo</th>
                                <th>Color</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($tarjetones as $itarj){?>
                            <tr>
                                <td><?php echo $itarj->vigenciaTarjeton; ?></td>
                                <td><?php echo $itarj->statusTarjeton; ?></td>
                                <td><?php echo $itarj->noTarjeton; ?></td>
                                <td><?php echo $itarj->tipoTarjeton; ?></td>
                                <td><?php echo $itarj->colorTarjeton; ?></td>  
                                <td>
                                    <a href="<?php echo base_url().'index.php/Tarjeton/editTarjetones/'.$itarj->idTarjeton;?>" class="btn btn-success"><i class="fa fa-edit"></i></a>
                                    <a href="<?php echo base_url().'index.php/Tarjeton/editStatusTarjetones/'.$itarj->idTarjeton;?>" class="btn btn-danger"><i class="fa fa-share"></i></a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>                        
                    </table>
                </div>
            </div>

            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                  </div>
                  <div class="modal-body">
                    
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>

     <script src="<?php echo base_url();?>js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/chart.min.js"></script>
    <script src="<?php echo base_url();?>js/chart-data.js"></script>
    <script src="<?php echo base_url();?>js/easypiechart.js"></script>
    <script src="<?php echo base_url();?>js/easypiechart-data.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>js/custom.js"></script>    
</body>
</html>
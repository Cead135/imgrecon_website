﻿
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>ImgRecon-Home</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:url" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:title" content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:image" content="">

  <link href="favicon.ico" rel="shortcut icon">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/lib/animate-css/animate.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

</head>

<body>
  <div id="preloader"></div>
    <section id="hero">
      <div class="hero-container">
        <div class="wow fadeIn">
          <h1>ImgRecon</h1>
            <h2> <span class="rotating">Control, Seguridad, Confianza, Inovación</span> </h2>
              <div class="actions">
                <a href="#about" class="btn-get-started">¿Quienes somos?</a>         
              </div>
        </div>
      </div>
    </section>

  <header id="header">
    <div class="container">
      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#hero">Inicio</a></li>
          <li><a href="#about">¿Quienes somos?</a></li>
          <li><a href="#team">Equipo de trabajo</a></li>
          <li><a href="<?php echo base_url().'index.php/Login';?>">Log in</a></li>

        </ul>
      </nav>
    </div>
  </header>

  <section id="about">
    <div class="container wow fadeInUp">
      <div class="row">
        <div class="col-md-12">
          <h3 class="section-title">¿Quienes somos?</h3>
          <div class="section-title-divider"></div> 
          <br>
        </div>
      </div>
    </div>
    <div class="container about-container wow fadeInUp">
      <div class="row">

        <div class="col-lg-6 about-img">
          <img src="<?php echo base_url();?>assets/img/salida.jpg" alt="">
        </div>

        <div class="col-md-6 about-content">
          <h2 class="about-title">Nuestro trabajo es tu seguridad</h2>
          <p class="about-text">
            Somos una empresa dirigida a crear softwares con nueva tecnología, para brindarle 
            a las instituciones y empresa una seguridad óptima, donde se pueda tener un 
            control total de los accesos de carros a los estacionamientos para que las mismas 
            personas de dicha empresa o institución se sientan seguros cuando dejan sus carros 
            mientras hacen sus labores. 
          </p>
          <br>
          <p class="about-text">
            Así que nuestra tarea es crear un sistema web donde se tenga un control de todas 
            las personas que trabajan dentro, las personas externas que hacen visitas he incluso 
            poder reportar si hay algún incidente.
          </p>
        </div>
      </div>
    </div>
  </section>

  
  <section id="team">
    <div class="container wow fadeInUp">
      <div class="row">
        <div class="col-md-12">
          <h3 class="section-title">Nuestro equipo de trabajo</h3>
          <div class="section-title-divider"></div>
          <p class="section-description">Estamos conformados por nuestro siguiente equipo de trabajo</p>
        </div>
      </div>

      <div class="row">

        <div class="col-md-4">
          <div class="member">
            <div class="pic"><img src="<?php echo base_url();?>assets/img/abdi.jpeg" alt=""></div>
            <h4>Cesár Abdi Ramos Peréz</h4>
            <span>Jefe de equipo</span>
            
          </div>
        </div>

        <div class="col-md-4">
          <div class="member">
            <div class="pic"><img src="<?php echo base_url();?>assets/img/stef.jpg" alt=""></div>
            <h4>Gerardo Stefan Miranda Escamilla</h4>
            <span>Analista y Programador Web</span>
          
          </div>
        </div>


        <div class="col-md-4">
          <div class="member">
            <div class="pic"><img src="<?php echo base_url();?>assets/img/edgar.jpeg" alt=""></div>
            <h4>Edgar Espinosa Guerreo</h4>
            <span>Analista y Programador Web</span>
          
          </div>
        </div>

      </div>
    </div>
  </section>

  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          
          <div class="credits">

            Designed by <a>ImgRecon</a>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa"></i></a>
  <script src="<?php echo base_url();?>assets/lib/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/superfish/hoverIntent.js"></script>
  <script src="<?php echo base_url();?>assets/lib/superfish/superfish.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/morphext/morphext.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/wow/wow.min.js"></script>
  <script src="<?php echo base_url();?>assets/lib/stickyjs/sticky.js"></script>
  <script src="<?php echo base_url();?>assets/lib/easing/easing.js"></script>
  <script src="<?php echo base_url();?>assets/js/custom.js"></script>
  <script src="<?php echo base_url();?>assets/contactform/contactform.js"></script>
</body>

</html>

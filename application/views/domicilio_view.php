<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ImgRecon</title>
    <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/datepicker3.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/styles.css" rel="stylesheet">
</head>

<body>
   <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" ><span>Img</span>Recon</a>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <em class="fa fa-envelope"></em>
                            <span class="label label-danger"></span>
                        </a>

                        <ul class="dropdown-menu dropdown-messages">
                            <li>
                                <div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="<?php echo base_url();?>/placehold.it/40/30a5ff/fff">
                                    </a>
                                    <div class="message-body"><small class="pull-right">3 mins ago</small>
                                        <a href="#"><strong>John Doe</strong> commented on <strong>your photo</strong>.</a>
                                    <br /><small class="text-muted">1:24 pm - 25/03/2015</small></div>
                                </div>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="<?php echo base_url();?>/placehold.it/40/30a5ff/fff">
                                    </a>
                                    <div class="message-body"><small class="pull-right">1 hour ago</small>
                                        <a href="#">New message from <strong>Jane Doe</strong>.</a>
                                    <br /><small class="text-muted">12:27 pm - 25/03/2015</small></div>
                                </div>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="all-button"><a href="#">
                                    <em class="fa fa-inbox"></em> <strong>All Messages</strong>
                                </a></div>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <em class="fa fa-bell"></em><span class="label label-info"></span>
                    </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li><a href="#">
                                <div><em class="fa fa-envelope"></em> 1 New Message
                                    <span class="pull-right text-muted small"></span></div>
                            </a></li>
                        </ul>
                    </li>

                    <li class="dropdown"><a href="login.html" class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <em class="fa fa-power-off"></em><span class="label label-info"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                            <li><a href="#">
                                <div><em class="fa fa-edit"></em> EDITAR PERFIL
                                    <span class="pull-right text-muted small"></span></div>
                            </a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo base_url().'index.php/Login/closeSession';?>">
                                <div><em class="fa fa-user"></em> CERRAR SESIÓN<font color="#286bcc"><?php echo $session['email'];?></font>
                                    <span class="pull-right text-muted small"></span></div>
                            </a></li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
    </nav>

    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar"> 
    <ul class="nav menu">           
        <li><a href="<?php echo base_url().'index.php/Cargo';?>"><em class="fa fa-clone">&nbsp;</em> Cargos </a></li>
        <li><a href="<?php echo base_url().'index.php/Area';?>"><em class="fa fa-clone">&nbsp;</em> Areas </a></li>    
        <li><a href="<?php echo base_url().'index.php/Carrera';?>"><em class="fa fa-clone">&nbsp;</em> Carreras </a></li>
        <li><a href="<?php echo base_url().'index.php/Grupo';?>"><em class="fa fa-clone">&nbsp;</em> Grupos </a></li>
        <li><a href="<?php echo base_url().'index.php/Tarjeton';?>"><em class="fa fa-clone">&nbsp;</em> Tarjetones </a></li>
        <li><a href="<?php echo base_url().'index.php/Tipoiden';?>"><em class="fa fa-clone">&nbsp;</em> Identificaciones </a></li>
        <li><a href="<?php echo base_url().'index.php/Domicilio';?>"><em class="fa fa-clone">&nbsp;</em> Domicilio </a></li>
        <li><a href="<?php echo base_url().'index.php/personas_control';?>"><em class="fa fa-clone">&nbsp;</em> Personas </a></li>
        <li><a href="<?php echo base_url().'index.php/Alumno';?>"><em class="fa fa-clone">&nbsp;</em> Alumnos </a></li>
        <li><a href="<?php echo base_url().'index.php/Empleado';?>"><em class="fa fa-clone">&nbsp;</em> Empleados </a></li>
        <li><a href="<?php echo base_url().'index.php/Visitante';?>"><em class="fa fa-clone">&nbsp;</em> Visitantes </a></li>
    </ul>
    </div>
        
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li class="active">UPSRJ SECURITY</li>
            </ol>
        </div>
        
<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Domicilios</h1>
    </div>
</div>

 <div class="row">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Nuevo domicilio</div>
            <div class="panel-body">

            <?php echo validation_errors();?>
            
            <?php if($this->session->flashdata('domicilioSaveSuccess')!=null){ ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('domicilioSaveSuccess');?>
                    </div>
            <?php } ?>


            <?php echo form_open_multipart('Domicilio/saveDomicilio',array('id'=>"domicilioForm"))?>
                <input type="hidden" name="personaDomiclio" id="personaDomiclio" value="<?php if(isset($persona)){echo $persona->idPersona;}?>">
                <?php if(isset($domicilioToEdit)){?>
                    <input type="hidden" name="idDomicilio" value="<?php echo $domicilioToEdit->idDomicilio?>">
                <?php } ?>

                
                       
                    <div class="col-xs-12">
                        <?php if(form_error('calleDomicilio')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('calleDomicilio');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>Calle</label>
                            <input type="text" name="calleDomicilio" id="calleDomicilio" class="form-control" value="<?php if(isset($domicilioToEdit)){ echo $domicilioToEdit->calleDomicilio;}?>">
                                <p class="help-block">Ejemplo: Av. 5</p>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <?php if(form_error('noIntDomicilio')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('noIntDomicilio');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>Numero interior (opcional)</label>
                            <input type="text" name="noIntDomicilio" id="noIntDomicilio" class="form-control" value="<?php if(isset($domicilioToEdit)){ echo $domicilioToEdit->noIntDomicilio;}?>">
                                <p class="help-block">Ejemplo: 65</p>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <?php if(form_error('noExtDomicilio')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('noExtDomicilio');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>Numero exterior</label>
                            <input type="text" name="noExtDomicilio" id="noExtDomicilio" class="form-control" value="<?php if(isset($domicilioToEdit)){ echo $domicilioToEdit->noExtDomicilio;}?>">
                                <p class="help-block">Ejemplo: 56</p>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <?php if(form_error('colonia')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('colonia');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>Colonia</label>
                            <input type="text" name="colonia" id="colonia" class="form-control" value="<?php if(isset($domicilioToEdit)){ echo $domicilioToEdit->colonia;}?>">
                                <p class="help-block">Ejemplo: Obrera</p>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <?php if(form_error('municipio')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('municipio');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>Municipio</label>
                            <input type="text" name="municipio" id="municipio" class="form-control" value="<?php if(isset($domicilioToEdit)){ echo $domicilioToEdit->municipio;}?>">
                                <p class="help-block">Ejemplo: Queretaro</p>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <?php if(form_error('estado')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('municipio');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>Estado</label>
                            <input type="text" name="estado" id="estado" class="form-control" value="<?php if(isset($domicilioToEdit)){ echo $domicilioToEdit->estado;}?>">
                                <p class="help-block">Ejemplo: Queretaro</p>
                        </div>
                    </div> 
                    <div class="col-xs-8">
                        <button type="reset" class="btn btn-danger">Cancelar</button>                             
                    </div>
                    <div class="col-xs-2">   
                        <button type="submit" class="btn btn-success">Guardar</button>    
                    </div>
                <?php echo form_close();?>        
            </div>
        </div>
    </div>
</div>
            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped table-bordered table-hover" id="domicilioTable"> 
                        <thead>
                            <tr>
                                <th>Calle</th>
                                <th>Numero Interior</th>
                                <th>Numero Exterior</th>
                                <th>Colonia</th>
                                <th>Municipio</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($domicilio as $iDom){?>
                            <tr>
                                <td><?php echo $iDom->calleDomicilio; ?></td>
                                <td><?php echo $iDom->noIntDomicilio; ?></td>
                                <td><?php echo $iDom->noExtDomicilio; ?></td>
                                <td><?php echo $iDom->colonia; ?></td>  
                                <td><?php echo $iDom->municipio; ?></td>
                                <td><?php echo $iDom->estado; ?></td>
                                <td>
                                    <a href="<?php echo base_url().'index.php/Domicilio/editDomicilio/'.$iDom->idDomicilio;?>" class="btn btn-success"><i class="fa fa-edit"></i></a>
                                    
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>                        
                    </table>
                </div>
            </div>

            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                  </div>
                  <div class="modal-body">
                    
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>
    <!-- /#wrapper -->
     <script src="<?php echo base_url();?>js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/chart.min.js"></script>
    <script src="<?php echo base_url();?>js/chart-data.js"></script>
    <script src="<?php echo base_url();?>js/easypiechart.js"></script>
    <script src="<?php echo base_url();?>js/easypiechart-data.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>js/custom.js"></script>
    <script>
        window.onload = function () {
    var chart1 = document.getElementById("line-chart").getContext("2d");
    window.myLine = new Chart(chart1).Line(lineChartData, {
    responsive: true,
    scaleLineColor: "rgba(0,0,0,.2)",
    scaleGridLineColor: "rgba(0,0,0,.05)",
    scaleFontColor: "#c5c7cc"
    });
};
    </script>
    
        
</body>
</html>
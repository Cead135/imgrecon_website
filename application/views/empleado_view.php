<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ImgRecon</title>
    <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/datepicker3.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/styles.css" rel="stylesheet">
</head>

<body>
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" ><span>Img</span>Recon</a>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <em class="fa fa-envelope"></em>
                            <span class="label label-danger"></span>
                        </a>

                        <ul class="dropdown-menu dropdown-messages">
                            <li>
                                <div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="<?php echo base_url();?>/placehold.it/40/30a5ff/fff">
                                    </a>
                                    <div class="message-body"><small class="pull-right">3 mins ago</small>
                                        <a href="#"><strong>John Doe</strong> commented on <strong>your photo</strong>.</a>
                                    <br /><small class="text-muted">1:24 pm - 25/03/2015</small></div>
                                </div>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="<?php echo base_url();?>/placehold.it/40/30a5ff/fff">
                                    </a>
                                    <div class="message-body"><small class="pull-right">1 hour ago</small>
                                        <a href="#">New message from <strong>Jane Doe</strong>.</a>
                                    <br /><small class="text-muted">12:27 pm - 25/03/2015</small></div>
                                </div>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <div class="all-button"><a href="#">
                                    <em class="fa fa-inbox"></em> <strong>All Messages</strong>
                                </a></div>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <em class="fa fa-bell"></em><span class="label label-info"></span>
                    </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li><a href="#">
                                <div><em class="fa fa-envelope"></em> 1 New Message
                                    <span class="pull-right text-muted small"></span></div>
                            </a></li>
                        </ul>
                    </li>

                    <li class="dropdown"><a href="login.html" class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <em class="fa fa-power-off"></em><span class="label label-info"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                            <li><a href="#">
                                <div><em class="fa fa-edit"></em> EDITAR PERFIL
                                    <span class="pull-right text-muted small"></span></div>
                            </a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo base_url().'index.php/Login/closeSession';?>">
                                <div><em class="fa fa-user"></em> CERRAR SESIÓN<font color="#286bcc"><?php echo $session['email'];?></font>
                                    <span class="pull-right text-muted small"></span></div>
                            </a></li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
    </nav>

    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar"> 
    <ul class="nav menu">           
        <li><a href="<?php echo base_url().'index.php/Cargo';?>"><em class="fa fa-clone">&nbsp;</em> Cargos </a></li>
        <li><a href="<?php echo base_url().'index.php/Area';?>"><em class="fa fa-clone">&nbsp;</em> Areas </a></li>    
        <li><a href="<?php echo base_url().'index.php/Carrera';?>"><em class="fa fa-clone">&nbsp;</em> Carreras </a></li>
        <li><a href="<?php echo base_url().'index.php/Grupo';?>"><em class="fa fa-clone">&nbsp;</em> Grupos </a></li>
        <li><a href="<?php echo base_url().'index.php/Tarjeton';?>"><em class="fa fa-clone">&nbsp;</em> Tarjetones </a></li>
        <li><a href="<?php echo base_url().'index.php/Tipoiden';?>"><em class="fa fa-clone">&nbsp;</em> Identificaciones </a></li>
        <li><a href="<?php echo base_url().'index.php/Domicilio';?>"><em class="fa fa-clone">&nbsp;</em> Domicilio </a></li>
        <li><a href="<?php echo base_url().'index.php/personas_control';?>"><em class="fa fa-clone">&nbsp;</em> Personas </a></li>
        <li><a href="<?php echo base_url().'index.php/Alumno';?>"><em class="fa fa-clone">&nbsp;</em> Alumnos </a></li>
        <li><a href="<?php echo base_url().'index.php/Empleado';?>"><em class="fa fa-clone">&nbsp;</em> Empleados </a></li>
        <li><a href="<?php echo base_url().'index.php/Visitante';?>"><em class="fa fa-clone">&nbsp;</em> Visitantes </a></li>
    </ul>
    </div>
        
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li class="active">UPSRJ SECURITY</li>
            </ol>
        </div>        
<div id="page-wrapper">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Empleados</h1>
    </div>
</div>

 <div class="row">
    <div class="col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">Nuevo empleado</div>
            <div class="panel-body">

            <?php echo validation_errors();?>
            
            <?php if($this->session->flashdata('empleadoSaveSuccess')!=null){ ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('empleadoSaveSuccess');?>
                    </div>
            <?php } ?>


            <?php echo form_open_multipart('Empleado/saveEmpleado',array('id'=>"empleadoForm"))?>
                
                <?php if(isset($empleadoToEdit)){?>
                    <input type="hidden" name="idEmpleado" value="<?php echo $empleadoToEdit->idEmpleado?>">
                <?php } ?>

                
                       
                    <div class="col-xs-12">
                        <?php if(form_error('nombreEmpleado')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('nombreEmpleado');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>Nombre</label>
                            <input type="text" name="nombreEmpleado" id="nombreEmpleado" class="form-control" value="<?php if(isset($empleadoToEdit)){ echo $empleadoToEdit->nombreEmpleado;}else{if(isset($persona)){echo $persona->nombrePersona;}}?>">
                                <p class="help-block">Ejemplo: Juan</p>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <?php if(form_error('apellidosEmpleado')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('apellidosEmpleado');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>Apellidos</label>
                            <input type="text" name="apellidosEmpleado" id="apellidosEmpleado" class="form-control" value="<?php if(isset($empleadoToEdit)){ echo $empleadoToEdit->apellidosEmpleado;}else{if(isset($persona)){echo $persona->apellidosPersona;}}?>">
                                <p class="help-block">Ejemplo: Hernandez</p>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <?php if(form_error('fechaNacEmpleado')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('fechaNacEmpleado');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>Fecha de nacimiento</label>
                            <input type="date" name="fechaNacEmpleado" id="fechaNacEmpleado" class="form-control" value="<?php if(isset($empleadoToEdit)){ echo $empleadoToEdit->fechaNacEmpleado;}?>">
                                <p class="help-block">Ejemplo: 01/01/2000</p>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <?php if(form_error('generoEmpleado')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('generoEmpleado');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>Genero</label>
                        <select id="generoEmpleado" name="generoEmpleado" class="form-control">
                            <option value="">
                                Seleccionar
                            </option>
                            <option value="Hombre" <?php if(isset($empleadoToEdit)){if($empleadoToEdit->generoEmpleado=="Hombre"){echo "selected";}}?> <?php if(isset($persona)){if($persona->generoPersona=="Hombre"){echo "selected";}}?>>
                                Hombre
                            </option value="Mujer" <?php if(isset($empleadoToEdit)){if($empleadoToEdit->generoEmpleado=="Mujer"){echo "selected";}}?> <?php if(isset($persona)){if($persona->generoPersona=="Mujer"){echo "selected";}}?>>>
                            <option>
                                Mujer
                            </option>
                        </select>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <?php if(form_error('curpEmpleado')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('curpEmpleado');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>CURP</label>
                            <input type="text" name="curpEmpleado" id="curpEmpleado" class="form-control" value="<?php if(isset($empleadoToEdit)){ echo $empleadoToEdit->curpEmpleado;}?>">
                                <p class="help-block">Ejemplo: ERTE513TE513513</p>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <?php if(form_error('rfcEmpleado')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('rfcEmpleado');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>RFC</label>
                            <input type="text" name="rfcEmpleado" id="rfcEmpleado" class="form-control" value="<?php if(isset($empleadoToEdit)){ echo $empleadoToEdit->rfcEmpleado;}?>">
                                <p class="help-block">Ejemplo: ERTE513TE513513</p>
                        </div>
                    </div>
                    
                    <div class="col-xs-12">
                        <?php if(form_error('areaEmpleado')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('areaEmpleado');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>Area</label>
                        <select id="areaEmpleado" name="areaEmpleado" class="form-control">
                            <option value="">
                                Seleccionar
                            </option>
                            <?php foreach($areas as $iArea){?>
                            <option value="<?php echo $iArea->idArea;?>" <?php if(isset($empleadoToEdit)){if($empleadoToEdit->fkareaEmpleado==$iArea->idArea){echo "selected";}}?>><?php echo $iArea->nombreArea;?></option>
                            <?php } ?>
                        </select>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <?php if(form_error('carreraEmpleado')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('carreraEmpleado');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>Carrera</label>
                        <select id="carreraEmpleado" name="carreraEmpleado" class="form-control">
                            <option value="">
                                Seleccionar
                            </option>
                            <?php foreach($carreras as $iCar){?>
                            <option value="<?php echo $iCar->idCarrera; ?>" <?php if(isset($empleadoToEdit)){if($empleadoToEdit->fkCarreraEmpleado==$iCar->idCarrera){echo "selected";}}?>><?php echo $iCar->nombreCarrera;?></option>
                            <?php } ?>
                        </select>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <?php if(form_error('tipoIdEmpleado')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('tipoIdEmpleado');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>Tipo de identificacion</label>
                        <select id="tipoIdEmpleado" name="tipoIdEmpleado" class="form-control">
                            <option value="">
                                Seleccionar
                            </option>
                            <?php foreach($tipoId as $iId){?>
                            <option value="<?php echo $iId->idIdentificacion; ?>" <?php if(isset($empleadoToEdit)){if($empleadoToEdit->fktipoIdentificacion==$iId->idIdentificacion){echo "selected";}}?>><?php echo $iId->nombreIdentificacion; ?></option>
                            <?php } ?>
                        </select>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <?php if(form_error('codigoIdentificacion')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('codigoIdentificacion');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>Codigo de identificacion</label>
                            <input type="text" name="codigoIdentificacion" id="codigoIdentificacion" class="form-control" value="<?php if(isset($empleadoToEdit)){ echo $empleadoToEdit->codigoIdentificacion;}?>">
                                <p class="help-block">Ejemplo: A66212841</p>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <?php if(form_error('codigoEmpleado')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('codigoEmpleado');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>Codigo de empleado</label>
                            <input type="text" name="codigoEmpleado" id="codigoEmpleado" class="form-control" value="<?php if(isset($empleadoToEdit)){ echo $empleadoToEdit->codigoEmpleado;}?>">
                                <p class="help-block">Ejemplo: A66212841</p>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <?php if(form_error('cargoEmpleado')!=null){?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?php echo form_error('cargoEmpleado');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                        <label>Cargo</label>
                        <select id="cargoEmpleado" name="cargoEmpleado" class="form-control">
                            <option value="">
                                Seleccionar
                            </option>
                            <?php foreach($cargos as $iCargo){?>
                            <option value="<?php echo $iCargo->idCargoAdministrativo; ?>" <?php if(isset($empleadoToEdit)){if($empleadoToEdit->fkCargoEmpleado==$iCargo->idCargoAdministrativo){echo "selected";}}?>><?php echo $iCargo->NombreCargo; ?></option>
                            <?php } ?>
                        </select>
                        </div>
                    </div>

                   <input type="hidden" name="personaEmpleado" value="<?php if(isset($persona)){echo $persona->idPersona;}?>">
                                       
                    <div class="col-xs-8">
                        <button type="reset" class="btn btn-danger">Cancelar</button>                             
                    </div>
                    <div class="col-xs-2">   
                        <button type="submit" class="btn btn-success">Guardar</button>    
                    </div>
                <?php echo form_close();?>        
            </div>
        </div>
    </div>
</div>


            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped table-bordered table-hover" id="empleadosTable"> 
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Apellidos</th>
                                <th>Fecha de nacimiento</th>
                                <th>Genero</th>
                                <th>CURP</th>
                                <th>RFC</th>
                                <th>Carrera</th>
                                <th>Area</th>
                                <th>Codigo</th>
                                <th>Tipo de ID</th>
                                <th>Codigo de ID</th>
                                <th>Persona</th>
                                <th>Editar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($empleados as $iempleados){?>
                            <tr>
                                <td><?php echo $iempleados->nombreEmpleado; ?></td>
                                <td><?php echo $iempleados->apellidosEmpleado; ?></td>
                                <td><?php echo $iempleados->fechaNacEmpleado; ?></td>
                                <td><?php echo $iempleados->generoEmpleado; ?></td>
                                <td><?php echo $iempleados->curpEmpleado; ?></td>
                                <td><?php echo $iempleados->RFC; ?></td>
                                <td><?php echo $iempleados->fkcarreraEmpleado; ?></td>
                                <td><?php echo $iempleados->fkareaEmpleado; ?></td>
                                <td><?php echo $iempleados->codigoEmpleado; ?></td>  
                                <td><?php echo $iempleados->fktipoIdentificacion; ?></td>
                                <td><?php echo $iempleados->codigoIdentificacion; ?></td>
                                <td><?php echo $iempleados->fkPersonaEmpleado; ?></td>
                                <td>
                                    <a href="<?php echo base_url().'index.php/Empleado/editEmpleados/'.$iempleados->idEmpleado;?>" class="btn btn-success"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>                        
                    </table>
                </div>
            </div>

            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                  </div>
                  <div class="modal-body">
                    
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>

     <script src="<?php echo base_url();?>js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/chart.min.js"></script>
    <script src="<?php echo base_url();?>js/chart-data.js"></script>
    <script src="<?php echo base_url();?>js/easypiechart.js"></script>
    <script src="<?php echo base_url();?>js/easypiechart-data.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>js/custom.js"></script>    
</body>
</html>
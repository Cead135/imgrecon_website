<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carreras_model extends CI_Model {

	public function saveCarreras($data){
		$this->db->insert('carrera',$data);
		return $this->db->insert_id();
	}

	public function loadCarreras(){
		$query = $this->db->get('carrera');
		return $query->result();
	} 

	public function getCarrerasById($idCarrera=NULL){
		if($idCarrera != NULL){
			$query = $this->db->get_where('carrera',array('idCarrera'=>$idCarrera));
			return $query->row();
		}else{
			return null;
		}
	}
	public function loadCarrerasById($idCarrera=NULL){
		if($idCarrera!=NULL){
			$query = $this->db->get_where('carrera',array('idCarrera'=>$idCarrera));
			return $query->row();
		} else{
			return NULL;
		}
	}
	public function editCarreras($data,$idCarrera){
		$this->db->where('idCarrera',$idCarrera);
		$this->db->update('carrera',$data);
	}
	public function editEstatus($data,$idCarrera){
		$this->db->where('idCarrera',$idCarrera);
		$this->db->update('carrera',$data);
	}

}
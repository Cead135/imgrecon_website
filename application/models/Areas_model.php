<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Areas_model extends CI_Model {

	public function saveAreas($data){
		$this->db->insert('area',$data);
		return $this->db->insert_id();
	}

	public function loadAreas(){
		$query = $this->db->get('area');
		return $query->result();
	} 

	public function getAreasById($idArea=NULL){
		if($idArea != NULL){
			$query = $this->db->get_where('area',array('idArea'=>$idArea));
			return $query->row();
		}else{
			return null;
		}
	}
	public function loadAreasById($idArea=NULL){
		if($idArea!=NULL){
			$query = $this->db->get_where('area',array('idArea'=>$idArea));
			return $query->row();
		} else{
			return NULL;
		}
	} 
	public function editAreas($data,$idArea){
		$this->db->where('idArea',$idArea);
		$this->db->update('area',$data);
	}
	public function editEstatus($data,$idArea){
		$this->db->where('idArea',$idArea);
		$this->db->update('area',$data);
	}

}
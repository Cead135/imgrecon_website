<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class alumno_model extends CI_Model {

	public function saveAlumno($data){
		$this->db->insert('alumno',$data);
		return $this->db->insert_id();
	}
	public function loadLastPersona()
	{
		$query = $this->db->query("SELECT * FROM persona ORDER BY idPersona DESC LIMIT 1");
		return $query->row();
	}

	public function loadAlumno(){
		$query = $this->db->get('alumno');
		return $query->result();
	} 
	public function loadCarreras(){
		$this->db->where('EstatusCarrera',"Activa");
		$query = $this->db->get('carrera');
		return $query->result();
	}
	public function loadGrupos(){
		$this->db->where('statusGrupo',"Activo");
		$query = $this->db->get('grupo');
		return $query->result();
	}
	public function loadIdentificaciones(){
		$this->db->where('statusIdentificacion',"Activo");
		$query = $this->db->get('tipoidentificacion');
		return $query->result();
	}
	public function loadDomicilio(){
		$query = $this->db->get('domicilio');
		return $query->result();
	}
	public function loadPersonas(){
		$query = $this->db->get('persona');
		return $query->result();
	}
	public function getAlumnoById($idAlumno=NULL){
		if($idAlumno != NULL){
			$query = $this->db->get_where('alumno',array('idAlumno'=>$idAlumno));
			return $query->row();
		}else{
			return null;
		}
	}
	public function getPersonaById($idPersona=NULL){
		if($idPersona != NULL){
			$query = $this->db->get_where('persona',array('idPersona'=>$idPersona));
			return $query->row();
		}else{
			return null;
		}
	}
	public function loadAlumnoById($idAlumno=NULL){
		if($idAlumno!=NULL){
			$query = $this->db->get_where('alumno',array('idAlumno'=>$idAlumno));
			return $query->row();
		} else{
			return NULL;
		}
	}
	public function editAlumno($data,$idAlumno){
		$this->db->where('idAlumno',$idAlumno);
		$this->db->update('alumno',$data);	
	}
	public function editEstatus($data,$idAlumno){
		$this->db->where('idAlumno',$idAlumno);
		$this->db->update('alumno',$data);
	}

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

	public function authenticate($correoUsuario, $contraseñaUsuario){
		$query = $this->db->get_where('Usuario',array('correoUsuario'=>$correoUsuario));
		$resultado = $query->row();
		if($resultado){
			$this->load->library('bcrypt');
			if($this->bcrypt->check_password($contraseñaUsuario,$resultado->contraseñaUsuario)
				){
				return $resultado;
			}else{
				$this->session->set_flashdata('loginError','Password invalid!');
				return null;
			}
		}else{
			$this->session->set_flashdata('loginError','User not found');
			return null;
		}
		
	}


}
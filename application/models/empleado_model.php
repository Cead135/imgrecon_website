<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class empleado_model extends CI_Model {

	public function saveEmpleado($data){
		$this->db->insert('empleado',$data);
		return $this->db->insert_id();
	}
	public function loadLastPersona()
	{
		$query = $this->db->query("SELECT * FROM persona ORDER BY idPersona DESC LIMIT 1");
		return $query->row();
	}

	public function loadEmpleado(){
		$query = $this->db->get('empleado');
		return $query->result();
	}
	public function loadAreas(){
		$this->db->where('statusArea',"Activa");
		$query = $this->db->get('area');
		return $query->result();
	}
	public function loadCargos(){
		$this->db->where('EstatusCargo',"Activo");
		$query = $this->db->get('cargoadministrativo');
		return $query->result();
	}
	public function loadCarreras(){
		$this->db->where('EstatusCarrera',"Activa");
		$query = $this->db->get('carrera');
		return $query->result();
	}
	public function loadGrupos(){
		$this->db->where('statusGrupo',"Activo");
		$query = $this->db->get('grupo');
		return $query->result();
	}
	public function loadIdentificaciones(){
		$this->db->where('statusIdentificacion',"Activo");
		$query = $this->db->get('tipoidentificacion');
		return $query->result();
	}
	public function loadDomicilio(){
		$query = $this->db->get('domicilio');
		return $query->result();
	}
	public function loadPersonas(){
		$query = $this->db->get('persona');
		return $query->result();
	}
	public function getEmpleadoById($idEmpleado=NULL){
		if($idEmpleado != NULL){
			$query = $this->db->get_where('empleado',array('idEmpleado'=>$idEmpleado));
			return $query->row();
		}else{
			return null;
		}
	}
	public function loadEmpleadoById($idEmpleado=NULL){
		if($idEmpleado!=NULL){
			$query = $this->db->get_where('empleado',array('idEmpleado'=>$idEmpleado));
			return $query->row();
		} else{
			return NULL;
		}
	}
	public function editEmpleado($data,$idEmpleado){
		$this->db->where('idEmpleado',$idEmpleado);
		$this->db->update('empleado',$data);	
	}
	public function editEstatus($data,$idEmpleado){
		$this->db->where('idEmpleado',$idEmpleado);
		$this->db->update('empleado',$data);
	}
	public function getPersonaById($idPersona=NULL){
		if($idPersona != NULL){
			$query = $this->db->get_where('persona',array('idPersona'=>$idPersona));
			return $query->row();
		}else{
			return null;
		}
	}

}
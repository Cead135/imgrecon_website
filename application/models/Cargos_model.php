<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cargos_model extends CI_Model {

	public function saveCargos($data){
		$this->db->insert('cargoadministrativo',$data);
		return $this->db->insert_id();
	}

	public function loadCargos(){
		$query = $this->db->get('cargoadministrativo');
		return $query->result();
	} 

	public function getCargosById($idCargoAdministrativo=NULL){
		if($idCargoAdministrativo != NULL){
			$query = $this->db->get_where('cargoadministrativo',array('idCargoAdministrativo'=>$idCargoAdministrativo));
			return $query->row();
		}else{
			return null;
		}
	}
	public function loadCargosById($idCargoAdministrativo=NULL){
		if($idCargoAdministrativo!=NULL){
			$query = $this->db->get_where('cargoadministrativo',array('idCargoAdministrativo'=>$idCargoAdministrativo));
			return $query->row();
		} else{
			return NULL;
		}
	} 
	public function editCargos($data,$idCargoAdministrativo){
		$this->db->where('idCargoAdministrativo',$idCargoAdministrativo);
		$this->db->update('cargoadministrativo',$data);
	}
	public function editEstatus($data,$idCargoAdministrativo){
		$this->db->where('idCargoAdministrativo',$idCargoAdministrativo);
		$this->db->update('cargoadministrativo',$data);
	}

}
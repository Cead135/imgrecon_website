<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grupos_model extends CI_Model {

	public function saveGrupos($data){
		$this->db->insert('grupo',$data);
		return $this->db->insert_id();
	}

	public function loadGrupos(){
		$query = $this->db->query("SELECT * FROM grupo, carrera WHERE fkcarrera= idCarrera");
		return $query->result();
	}
	public function loadCarreras(){
		$this->db->where('EstatusCarrera',"Activa");
		$query = $this->db->get('carrera');
		return $query->result();
	} 
	public function getGruposById($idGrupo=NULL){
		if($idGrupo != NULL){
			$query = $this->db->get_where('grupo',array('idGrupo'=>$idGrupo));
			return $query->row();
		}else{
			return null;
		}
	}
	public function loadGruposById($idGrupo=NULL){
		if($idGrupo!=NULL){
			$query = $this->db->get_where('grupo',array('idGrupo'=>$idGrupo));
			return $query->row();
		} else{
			return NULL;
		}
	}
	public function editGrupos($data,$idGrupo){
		$this->db->where('idGrupo',$idGrupo);
		$this->db->update('grupo',$data);
	}
	public function editEstatus($data,$idGrupo){
		$this->db->where('idGrupo',$idGrupo);
		$this->db->update('grupo',$data);
	}

}
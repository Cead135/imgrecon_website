<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mensajes_model extends CI_Model {

	public function saveMensajes($data){
		$this->db->insert('mensaje',$data);
		return $this->db->insert_id();
	}

	public function loadMensajes(){
		$query = $this->db->query("SELECT * FROM mensaje, persona WHERE fkPersonaMensaje= idPersona");
		return $query->result();
	} 

	public function loadPersonas(){
		//$this->db->where('EstatusCarrera',"Activa");
		$query = $this->db->get('persona');
		return $query->result();
	}

	public function getMensajesById($idMensaje=NULL){
		if($idMensaje != NULL){
			$query = $this->db->get_where('mensaje',array('idMensaje'=>$idMensaje));
			return $query->row();
		}else{
			return null;
		}
	}
	public function loadMensajesById($idMensaje=NULL){
		if($idMensaje!=NULL){
			$query = $this->db->get_where('mensaje',array('idMensaje'=>$idMensaje));
			return $query->row();
		} else{
			return NULL;
		}
	}

	public function editMensajes($data,$idMensaje){
		$this->db->where('idMensaje',$idMensaje);
		$this->db->update('mensaje',$data);
	}
	
	public function editEstatus($data,$idMensaje){
		$this->db->where('idMensaje',$idMensaje);
		$this->db->update('mensaje',$data);
	}

}
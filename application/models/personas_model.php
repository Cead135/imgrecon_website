
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class personas_model extends CI_Model {

	public function savePersonas($data){
		$this->db->insert('Persona',$data);
	}
	public function loadLastPersona()
	{
		$query = $this->db->query("SELECT * FROM persona ORDER BY idPersona DESC LIMIT 1");
		return $query->result();
	}
	public function loadPersonas(){
		$query=$this->db->get('Persona');

		//$query = $this->db->get_where('Games',array('gameStatus'=>'Active'));
		return $query->result();
	} 
	/*public function loadTiposIdentificacion(){
		$query=$this->db->get('TipoIdentificacion');

		//$query = $this->db->get_where('Games',array('gameStatus'=>'Active'));
		return $query->result();
	} */
	

	public function getPersonaById($idPersona=NULL){
		if($idPersona != NULL){
			$query = $this->db->get_where('Persona',array('idPersona'=>$idPersona));
			return $query->row();
		}else{
			return null;
		}
	}
	public function editPersonas($data,$idPersona){
		$this->db->where('idPersona',$idPersona);
		$this->db->update('Persona',$data);
	}

	/*public function deleteIdentificacion($idIdentificacion){
		$this->db->where('idIdentificacion',$idIdentificacion);
		$this->db->delete('Identificacion');
	}*/

	

}


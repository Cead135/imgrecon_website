<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class domicilio_model extends CI_Model {

	public function saveDomicilio($data){
		$this->db->insert('domicilio',$data);
		return $this->db->insert_id();
	}
	public function getPersonaById($idPersona=NULL){
		if($idPersona != NULL){
			$query = $this->db->get_where('Persona',array('idPersona'=>$idPersona));
			return $query->row();
		}else{
			return null;
		}
	}
	public function loadLastAddress()
	{
		$query = $this->db->query("SELECT * FROM domicilio ORDER BY idDomicilio DESC LIMIT 1");
		return $query->result();
	}
	public function asignfk($idEmpleado=NULL,$idDomicilio=NULL)
	{
		$data = array(
			"fkdomicilioEmpleado"=>$idDomicilio
			);
		$this->db->where('idEmpleado',$idEmpleado);
		$this->db->update('empleado',$data);
	}
	public function loadDomicilio(){
		$query = $this->db->get('domicilio');
		return $query->result();
	} 

	public function getDomicilioById($idDomicilio=NULL){
		if($idDomicilio != NULL){
			$query = $this->db->get_where('domicilio',array('idDomicilio'=>$idDomicilio));
			return $query->row();
		}else{
			return null;
		}
	}
	public function editDomicilio($data,$idDomicilio){
		$this->db->where('idDomicilio',$idDomicilio);
		$this->db->update('domicilio',$data);
		
	}

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tarjeton_model extends CI_Model {

	public function saveTarjeton($data){
		$this->db->insert('tarjeton',$data);
		return $this->db->insert_id();
	}

	public function loadTarjeton(){
		$query = $this->db->get('tarjeton');
		return $query->result();
	} 

	public function getTarjetonById($idTarjeton=NULL){
		if($idTarjeton != NULL){
			$query = $this->db->get_where('tarjeton',array('idTarjeton'=>$idTarjeton));
			return $query->row();
		}else{
			return null;
		}
	}
	public function loadTarjeById($idTarjeton=NULL){
		if($idTarjeton!=NULL){
			$query = $this->db->get_where('tarjeton',array('idTarjeton'=>$idTarjeton));
			return $query->row();
		} else{
			return NULL;
		}
	}
	public function editTarjeton($data,$idTarjeton){
		$this->db->where('idTarjeton',$idTarjeton);
		$this->db->update('tarjeton',$data);	
	}
	public function editEstatus($data,$idTarjeton){
		$this->db->where('idTarjeton',$idTarjeton);
		$this->db->update('tarjeton',$data);
	}

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipoiden_model extends CI_Model {

	public function saveIdentificaciones($data){
		$this->db->insert('tipoidentificacion',$data);
		return $this->db->insert_id();
	}

	public function loadIdentificaciones(){
		$query = $this->db->get('tipoidentificacion');
		return $query->result();
	} 
	public function getIdentificacionesById($idIdentificacion=NULL){
		if($idIdentificacion != NULL){
			$query = $this->db->get_where('tipoidentificacion',array('idIdentificacion'=>$idIdentificacion));
			return $query->row();
		}else{
			return null;
		}
	}
	public function loadIdenById($idIdentificacion=NULL){
		if($idIdentificacion!=NULL){
			$query = $this->db->get_where('tipoidentificacion',array('idIdentificacion'=>$idIdentificacion));
			return $query->row();
		} else{
			return NULL;
		}
	}
	public function editIdentificaciones($data,$idIdentificacion){
		$this->db->where('idIdentificacion',$idIdentificacion);
		$this->db->update('tipoidentificacion',$data);
	}
	public function editEstatus($data,$idIdentificacion){
		$this->db->where('idIdentificacion',$idIdentificacion);
		$this->db->update('tipoidentificacion',$data);
	}

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class visitante_model extends CI_Model {

	public function saveVisitante($data){
		$this->db->insert('visitante',$data);
		return $this->db->insert_id();
	}
	public function loadLastPersona()
	{
		$query = $this->db->query("SELECT * FROM persona ORDER BY idPersona DESC LIMIT 1");
		return $query->row();
	}

	public function loadVisitante(){
		$query = $this->db->get('visitante');
		return $query->result();
	}
	public function loadAreas(){
		$this->db->where('statusArea',"Activa");
		$query = $this->db->get('area');
		return $query->result();
	}
	public function loadIdentificaciones(){
		$this->db->where('statusIdentificacion',"Activo");
		$query = $this->db->get('tipoidentificacion');
		return $query->result();
	}
	public function loadPersonas(){
		$query = $this->db->get('persona');
		return $query->result();
	}
	public function getVisitanteById($idVisitante=NULL){
		if($idVisitante != NULL){
			$query = $this->db->get_where('visitante',array('idVisitante'=>$idVisitante));
			return $query->row();
		}else{
			return null;
		}
	}
	public function loadVisitanteById($idVisitante=NULL){
		if($idVisitante!=NULL){
			$query = $this->db->get_where('visitante',array('idVisitante'=>$idVisitante));
			return $query->row();
		} else{
			return NULL;
		}
	}
	public function editVisitante($data,$idVisitante){
		$this->db->where('idVisitante',$idVisitante);
		$this->db->update('visitante',$data);	
	}
	public function getPersonaById($idPersona=NULL){
		if($idPersona != NULL){
			$query = $this->db->get_where('persona',array('idPersona'=>$idPersona));
			return $query->row();
		}else{
			return null;
		}
	}

}